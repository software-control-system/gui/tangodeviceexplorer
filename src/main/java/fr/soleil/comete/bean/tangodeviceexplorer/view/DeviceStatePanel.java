package fr.soleil.comete.bean.tangodeviceexplorer.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.cdma.gui.databrowser.DataBrowserItemEvent;
import org.cdma.gui.databrowser.interfaces.IDataBrowserItemListener;

import fr.soleil.comete.bean.tangodeviceexplorer.controller.TangoDeviceExplorerController;
import fr.soleil.comete.swing.TextArea;
import fr.soleil.data.service.IKey;

public class DeviceStatePanel extends AbstractDeviceExplorerPanel implements IDataBrowserItemListener {

    private static final long serialVersionUID = -4353999649648021470L;

    private JPanel statePanel;
    private JPanel onlyStatePanel;
    private JPanel commandsPanel;
    private TextArea statusArea;
    private JLabel deviceNameLabel;
    private JComboBox<String> comboCommands;

    private TangoDeviceExplorerController controller = null;

    public DeviceStatePanel(TangoDeviceExplorerController controller) {
        this.controller = controller;
        controller.addDataBrowserItemListener(this);
        initComponent();
    }

    public void setStatusVisible(boolean visible) {
        if (statusArea != null) {
            statusArea.setVisible(visible);
        }
    }

    private void initComponent() {
        setLayout(new BorderLayout());

        String deviceName = controller.getDeviceName();

        statePanel = new JPanel();
        onlyStatePanel = new JPanel();
        commandsPanel = new JPanel();
        deviceNameLabel = new JLabel();

        statusArea = new TextArea();

        comboCommands = new JComboBox<>();
        comboCommands.setEnabled(false);
        comboCommands.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getModifiers() != 0 && comboCommands.isEnabled()) {
                    Object selected = comboCommands.getSelectedItem();
                    if (selected != null) {
                        controller.executeCommand(selected.toString());
                    }
                }
            }
        });

        // Create command panel
        statePanel.setLayout(new BorderLayout());
        onlyStatePanel.setLayout(new BorderLayout());
        onlyStatePanel.add(getStateLabel(), BorderLayout.CENTER);
        statePanel.add(onlyStatePanel, BorderLayout.CENTER);
        commandsPanel.add(deviceNameLabel);
        commandsPanel.add(comboCommands);
        deviceNameLabel.setText(deviceName);

        statePanel.add(commandsPanel, BorderLayout.SOUTH);

        add(statePanel, BorderLayout.NORTH);
        statusArea.setBorder(new EmptyBorder(10, 10, 10, 10));
        add(statusArea, BorderLayout.CENTER);
    }

    public void removeAllCommand() {
        if (comboCommands != null) {
            comboCommands.removeAllItems();
        }
        comboCommands.setEnabled(false);
    }

    public void addCommand(final String commandName) {
        if (comboCommands != null && commandName != null) {
            int size = comboCommands.getItemCount();
            if (size == 0) {
                comboCommands.addItem(commandName);
            } else {
                boolean exists = false;
                for (int i = 0; i < size; i++) {
                    exists = commandName.equals(comboCommands.getItemAt(i));
                    if (exists) {
                        break;
                    }
                }
                if (!exists) {
                    comboCommands.addItem(commandName);
                }
            }

            int newsize = comboCommands.getItemCount();
            if (newsize > 0) {
                comboCommands.setEnabled(true);
            }
        }
    }

    public List<String> getOpenedCommand() {
        List<String> openedCommand = new ArrayList<String>();
        if (comboCommands != null) {
            int itemCount = comboCommands.getItemCount();
            for (int i = 0; i < itemCount; i++) {
                openedCommand.add(String.valueOf(comboCommands.getItemAt(i)));
            }
        }
        return openedCommand;
    }

    public void setCommandTransferHandler(TreeCommandTransferHandler transferHandler) {
        comboCommands.setTransferHandler(transferHandler);
    }

    @Override
    protected void clearGUI() {
        cleanStateModel();
        cleanWidget(statusArea);
    }

    @Override
    protected void refreshGUI() {
        setStateModel();
        IKey statusKey = generateAttributeKey("Status");
        stringBox.setColorEnabled(statusArea, false);
        setWidgetModel(statusArea, stringBox, statusKey);
    }

    @Override
    public void onItemOpened(DataBrowserItemEvent event) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onItemClosed(DataBrowserItemEvent event) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onItemSelected(DataBrowserItemEvent event) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onItemDeselected(DataBrowserItemEvent event) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onItemAxisChanged(DataBrowserItemEvent event) {
        // TODO Auto-generated method stub
    }

}
