package fr.soleil.comete.bean.tangodeviceexplorer.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DispLevel;
import fr.esrf.TangoApi.CommandInfo;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.comete.bean.tangodeviceexplorer.controller.TangoDeviceExplorerController;
import fr.soleil.comete.bean.tangodeviceexplorer.view.TangoDeviceExplorerUtilities;
import fr.soleil.comete.tango.data.service.helper.TangoCommandHelper;
import fr.soleil.comete.tango.data.service.helper.TangoConstHelper;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.tango.clientapi.TangoCommand;

public class TangoCommandSourceDevice extends AbstractTangoDeviceSource {
    private static final String COMMAND_NOT_FOUND_ERROR = "Command is not found";
    private static final String COMMAND_EXECUTION_ERROR = "Command execution failed";
    private static final String COMMAND_ARGIN_ERROR = "Argin is null or empty";
    private static final String RESULT_SEP = "\n";
    private static final String TABULATION = "\t";
    private static final String NUMBER_VALUE = "[VALUE\t";
    private static final String STRING_VALUE = "STRING]";
    public final static String COMMANDS_NODE = "COMMANDS";

    private static Map<String, TangoCommand> commandMap = new HashMap<String, TangoCommand>();
    private static Map<String, CommandInfo> commandInfoMap = new HashMap<String, CommandInfo>();

    List<String> commandList = new ArrayList<String>();
    List<String> commandListNode = new ArrayList<String>();

    public TangoCommandSourceDevice(final TangoDeviceExplorerController controller) {
        super(controller);
    }

    public boolean needArgin(String command) {
        boolean argin = false;
        try {
            TangoCommand tangoCommand = getTangoCommand(command);
            if (tangoCommand != null) {
                argin = !tangoCommand.isArginVoid();
            }
        } catch (Exception e) {
        }
        return argin;
    }

    public Object executeVoidCommand(final String command) {
        Object result = null;
        try {
            TangoCommand tangoCommand = getTangoCommand(command);
            // If the command is not null
            if (tangoCommand != null) {
                // Execution of the command
                tangoCommand.execute();
                if (haveArgout(command)) {
                    result = tangoCommand.extractToString(RESULT_SEP);
                }
            }
        } catch (DevFailed devFailed) {
            Exception ex = new Exception(COMMAND_EXECUTION_ERROR + " " + controller.getDeviceName() + "/" + command
                    + ":" + DevFailedUtils.toString(devFailed), devFailed);
            controller.traceError(COMMAND_EXECUTION_ERROR + "{}/{} :", ex, controller.getDeviceName(), command);
            result = ex;
        } catch (Exception ex) {
            result = ex;
        }
        return result;
    }

    public Object executeCommand(final String command, Object argin) {
        Object result = null;
        if (argin != null && !String.valueOf(argin).isEmpty()) {
            try {
                TangoCommand tangoCommand = getTangoCommand(command);
                // If the command is not null
                if (tangoCommand != null) {
                    // Format scalar
                    if (tangoCommand.isArginScalar()) {
                        tangoCommand.execute(argin);
                    }
                    // Format spectrum
                    else if (tangoCommand.isArginSpectrum() && argin instanceof String[]) {
                        int commandInType = TangoCommandHelper.getCommandInType(controller.getDeviceName(), command);
                        int format = TangoConstHelper.getTangoFormat(commandInType);
                        Object[] valueArray = (String[]) argin;
                        switch (format) {
                            case TangoConstHelper.STRING_FORMAT:
                                tangoCommand.execute(valueArray);
                                break;

                            case TangoConstHelper.NUMERICAL_FORMAT:
                                if (!(result instanceof Exception)) {
                                    tangoCommand.execute(Double[].class, valueArray);
                                }
                                break;

                            case TangoConstHelper.BOOLEAN_FORMAT:
                                tangoCommand.execute(Boolean[].class, valueArray);
                                break;
                        }
                    }

                    // Format mixed
                    else if (tangoCommand.isArginMixFormat() && argin instanceof Object[]) {
                        if (argin instanceof Object[]) {
                            Object[] objectArray = (Object[]) argin;
                            String[] stringArray = (String[]) objectArray[0];
                            String[] valueArray = (String[]) objectArray[1];
                            tangoCommand.insertMixArgin(valueArray, stringArray);
                            tangoCommand.execute();
                        }
                    }

                    if (haveArgout(command) && !(result instanceof Exception)) {
                        if (!tangoCommand.isArgoutMixFormat()) {
                            result = tangoCommand.extractToString(RESULT_SEP);
                        } else {
                            String[] numMixArrayArgout = tangoCommand.getNumMixArrayArgout();
                            String[] stringMixArrayArgout = tangoCommand.getStringMixArrayArgout();
                            StringBuilder builder = new StringBuilder();
                            builder.append(NUMBER_VALUE).append(STRING_VALUE).append(RESULT_SEP);
                            for (int i = 0; i < stringMixArrayArgout.length; i++) {
                                builder.append(
                                        numMixArrayArgout[i] + TABULATION + stringMixArrayArgout[i] + RESULT_SEP);
                            }
                            result = builder.toString();
                        }
                    }
                }
            } catch (DevFailed devFailed) {
                Exception ex = new Exception(COMMAND_EXECUTION_ERROR + " " + controller.getDeviceName() + "/" + command
                        + ":" + DevFailedUtils.toString(devFailed), devFailed);
                controller.traceError(COMMAND_EXECUTION_ERROR + "{}/{} :", ex, controller.getDeviceName(), command);
                result = ex;
            } catch (Exception ex) {
                result = ex;
            }
        } else {
            Exception ex = new Exception(COMMAND_ARGIN_ERROR);
            controller.traceError(COMMAND_ARGIN_ERROR + "{}/{} :", ex, controller.getDeviceName(), command);
            result = ex;
        }

        return result;
    }

    public TangoCommand getTangoCommand(final String command) throws Exception {
        TangoCommand tangoCommand = null;

        if (command != null && !command.isEmpty()) {
            // Test if the command is already created
            tangoCommand = commandMap.get(command.toLowerCase());
            // If it is not created
            if (tangoCommand == null) {
//                tangoCommand = TangoCommandHelper.getTangoCommand(controller.getDeviceName(), command);
                // XXX The use of TangoCommand should be avoided
                tangoCommand = new TangoCommand(controller.getDeviceName(), command);
                if (tangoCommand != null) {
                    // Add to the commandMap
                    commandMap.put(command.toLowerCase(), tangoCommand);
                }
            }
        } else {
            Exception exc = new Exception(COMMAND_NOT_FOUND_ERROR);
            controller.traceError(COMMAND_NOT_FOUND_ERROR, exc);
            throw exc;
        }

//        if (tangoCommand == null) {
//            Exception ex = new Exception(COMMAND_NOT_FOUND_ERROR + " " + controller.getDeviceName() + "/" + command);
//            controller.traceError(ex.getMessage(), ex);
//            throw ex;
//        }
        return tangoCommand;
    }

    public String getCommandDescription(final String command) {
        String description = descriptionMap.get(command.toLowerCase());
        if (description == null && commandInfoMap.containsKey(command.toLowerCase())) {
            boolean needArgin = needArgin(command);
            boolean haveArgout = haveArgout(command);
            if (needArgin || haveArgout) {
                StringBuilder builder = new StringBuilder();
                builder.append("<HTML>");
                CommandInfo commandInfo = commandInfoMap.get(command.toLowerCase());
                if (needArgin) {
                    builder.append("<B><U>Input description :</B></U><BR>");
                    builder.append(commandInfo.in_type_desc.replaceAll("\n", "<BR>"));
                }
                if (haveArgout) {
                    builder.append("<BR><B><U>Output description :</B></U><BR>");
                    builder.append(commandInfo.out_type_desc.replaceAll("\n", "<BR>"));
                }
                description = builder.toString();
                builder.append("</HTML>");
            } else {
                description = "";
            }
            descriptionMap.put(command.toLowerCase(), description);
        }
        return description;
    }

    private boolean haveArgout(String command) {
        boolean argout = false;
        int commandOutType = TangoCommandHelper.getCommandOutType(controller.getDeviceName(), command);
        argout = commandOutType != TangoConst.Tango_DEV_VOID;
        return argout;
    }

    public List<String> getCommandList() {
        return commandList;
    }

    public boolean containsCommand(final String command) {
        return commandMap.containsKey(command.toLowerCase());
    }

    @Override
    public void setExpertLevel(boolean expertLevel) {
        if (this.expertLevel != expertLevel) {
            super.setExpertLevel(expertLevel);
            commandList.clear();
            commandListNode.clear();
            Set<Entry<String, CommandInfo>> entrySet = commandInfoMap.entrySet();
            String commandName = null;
            CommandInfo commandInfo = null;
            for (Entry<String, CommandInfo> entry : entrySet) {
                commandInfo = entry.getValue();
                commandName = commandInfo.cmd_name;
                boolean add = (!expertLevel && commandInfo.level == DispLevel.OPERATOR) || expertLevel;
                if (add) {
                    commandListNode.add(COMMANDS_NODE + TangoDeviceExplorerUtilities.PATH_SEPARATOR + commandName);
                    commandList.add(entry.getKey());
                }
            }
        }
    }

    @Override
    public void createSourceList() {
        commandList.clear();
        commandListNode.clear();
        commandInfoMap.clear();
        commandMap.clear();

        String deviceName = controller.getDeviceName();
        String currentCommand = null;

        if (TangoDeviceHelper.isDeviceRunning(deviceName)) {
            DeviceProxy deviceProxy = TangoDeviceHelper.getDeviceProxy(deviceName);
            try {
                CommandInfo[] command_list_query = deviceProxy.command_list_query();

                for (CommandInfo commandInfo : command_list_query) {
                    currentCommand = commandInfo.cmd_name;
                    boolean add = (!expertLevel && commandInfo.level == DispLevel.OPERATOR) || expertLevel;
                    if (add) {
                        commandListNode
                                .add(COMMANDS_NODE + TangoDeviceExplorerUtilities.PATH_SEPARATOR + currentCommand);
                        commandList.add(currentCommand.toLowerCase());
                    }
                    commandInfoMap.put(currentCommand.toLowerCase(), commandInfo);
                }
            } catch (DevFailed e) {
                String errorMessage = DevFailedUtils.toString(e);
                Exception exc = new Exception(errorMessage, e);
                controller.traceError(errorMessage + "{}/{}", exc, deviceName, currentCommand);
            }
        }
    }

    @Override
    public List<String> getDataSourceList() {
        return commandListNode;
    }

    @Override
    public boolean contains(final String key) {
        return commandList.contains(key.toLowerCase());
    }
}
