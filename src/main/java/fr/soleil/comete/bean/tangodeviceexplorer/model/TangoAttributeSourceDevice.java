package fr.soleil.comete.bean.tangodeviceexplorer.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.cdma.gui.databrowser.impl.tango.TangoDataSourceBrowser;
import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.DataType;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DispLevel;
import fr.esrf.TangoApi.AttributeInfo;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.tangodeviceexplorer.controller.TangoDeviceExplorerController;
import fr.soleil.comete.bean.tangodeviceexplorer.view.TangoDeviceExplorerUtilities;
import fr.soleil.comete.tango.data.service.helper.TangoAttributeHelper;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

public class TangoAttributeSourceDevice extends AbstractTangoDeviceSource {

    List<String> attributeList = new ArrayList<String>();
    List<String> scalarList = new ArrayList<String>();
    List<String> spectrumList = new ArrayList<String>();
    List<String> imageList = new ArrayList<String>();
    List<String> attributeListNode = new ArrayList<String>();
    Map<String, AttributeInfo> attributeInfoMap = new HashMap<String, AttributeInfo>();
    Map<String, DataFormat> attributeFormatMap = new HashMap<String, DataFormat>();

    public TangoAttributeSourceDevice(final TangoDeviceExplorerController controller) {
        super(controller);
    }

    @Override
    public void setExpertLevel(boolean expertLevel) {
        if (this.expertLevel != expertLevel) {
            super.setExpertLevel(expertLevel);
            attributeList.clear();
            attributeListNode.clear();
            Set<Entry<String, AttributeInfo>> entrySet = attributeInfoMap.entrySet();
            String attributeName = null;
            AttributeInfo attributeInfo = null;
            DataType type = null;
            for (Entry<String, AttributeInfo> entry : entrySet) {
                attributeName = entry.getKey();
                attributeInfo = entry.getValue();
                boolean add = (!expertLevel && attributeInfo.level == DispLevel.OPERATOR) || expertLevel;
                if (add) {
                    type = getAttributeType(attributeInfo);
                    attributeListNode.add(type + TangoDeviceExplorerUtilities.PATH_SEPARATOR + attributeName);
                    attributeList.add(attributeName);
                }
            }
        }
    }

    @Override
    public void createSourceList() {
        attributeListNode.clear();
        attributeList.clear();
        attributeInfoMap.clear();
        attributeFormatMap.clear();
        String deviceName = controller.getDeviceName();
        String currentAttribute = null;
        if (TangoDeviceHelper.isDeviceRunning(deviceName)) {
            DeviceProxy deviceProxy = TangoDeviceHelper.getDeviceProxy(deviceName);
            try {
                String[] get_attribute_list = deviceProxy.get_attribute_list();
                AttributeInfo attributeInfo = null;
                DataType type = null;
                for (String attribute : get_attribute_list) {
                    currentAttribute = attribute;
                    attributeInfo = deviceProxy.get_attribute_info(attribute);
                    boolean add = (!expertLevel && attributeInfo.level == DispLevel.OPERATOR) || expertLevel;
                    if (add && (!attribute.equals("State") && (!attribute.equals("Status")))) {
                        type = getAttributeType(attributeInfo);

                        if (DataType.SCALAR.equals(type)) {
                            scalarList.add(attribute.toLowerCase());
                        } else if (DataType.SPECTRUM.equals(type)) {
                            spectrumList.add(attribute.toLowerCase());
                        } else if (DataType.IMAGE.equals(type)) {
                            imageList.add(attribute.toLowerCase());
                        }

                        attributeListNode.add(type + TangoDeviceExplorerUtilities.PATH_SEPARATOR + attribute);
                        attributeList.add(attribute.toLowerCase());
                    }
                    attributeInfoMap.put(attribute.toLowerCase(), attributeInfo);
                }
            } catch (DevFailed e) {
                String errorMessage = DevFailedUtils.toString(e);
                Exception exc = new Exception(errorMessage, e);
                controller.traceError(errorMessage + "{}/{}", exc, deviceName, currentAttribute);
            }
        }
    }

    @Override
    public List<String> getDataSourceList() {
        return attributeListNode;
    }

    public List<String> getAttributeList() {
        return attributeList;
    }
    
    public List<String> getScalarList() {
        return scalarList;
    }
    
    public List<String> getImageList() {
        return imageList;
    }
    
    public List<String> getSpectrumList() {
        return spectrumList;
    }

    public DataFormat getAttributeFormat(final String attribute) {
        DataFormat format = attributeFormatMap.get(attribute.toLowerCase());
        if (format == null && attributeInfoMap.containsKey(attribute.toLowerCase())) {
            AttributeInfo attributeInfo = attributeInfoMap.get(attribute.toLowerCase());
            int attributeFormat = TangoAttributeHelper.getAttributeFormat(attributeInfo);
            format = TangoDataSourceBrowser.getDataFormat(attributeFormat);
            attributeFormatMap.put(attribute.toLowerCase(), format);
        }
        return format;

    }

    private DataType getAttributeType(final AttributeInfo attributeInfo) {
        DataType type = null;
        int tangoType = TangoAttributeHelper.getAttributeType(attributeInfo);
        switch (tangoType) {
            case AttrDataFormat._SCALAR:
                type = DataType.SCALAR;
                break;

            case AttrDataFormat._SPECTRUM:
                type = DataType.SPECTRUM;
                break;

            case AttrDataFormat._IMAGE:
                type = DataType.IMAGE;
                break;
        }
        return type;
    }

    public String getAttributeDescription(final String attribute) {
        String description = descriptionMap.get(attribute.toLowerCase());
        if (description == null && attributeInfoMap.containsKey(attribute.toLowerCase())) {
            AttributeInfo attributeInfo = attributeInfoMap.get(attribute.toLowerCase());
            if (attributeInfo != null && attributeInfo.label != null) {
                StringBuilder builder = new StringBuilder();
                builder.append("<HTML>");
                builder.append(attributeInfo.label);
                if (attributeInfo.description != null && !attributeInfo.description.equalsIgnoreCase("No description")) {
                    builder.append("<BR><B><U>Description :</U></B><BR>");
                    builder.append(attributeInfo.description.replaceAll("\n", "<BR>"));
                }
                description = builder.toString();
                builder.append("</HTML>");
            } else {
                description = attribute;
            }
            descriptionMap.put(attribute.toLowerCase(), description);
        }
        return description;

    }

    @Override
    public boolean contains(final String key) {
        return attributeInfoMap.containsKey(key.toLowerCase());
    }

}
