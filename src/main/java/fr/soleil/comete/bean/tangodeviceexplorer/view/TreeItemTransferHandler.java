package fr.soleil.comete.bean.tangodeviceexplorer.view;

import java.util.List;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.AxisType;
import org.cdma.gui.databrowser.interfaces.DataType;
import org.cdma.gui.databrowser.util.ItemTransferHandler;

import fr.soleil.comete.bean.tangodeviceexplorer.controller.TangoDeviceExplorerController;
import fr.soleil.data.service.IKey;

public class TreeItemTransferHandler extends ItemTransferHandler {

    private static final long serialVersionUID = -7850392434215551635L;
    private DeviceTreePanel tree;

    public TreeItemTransferHandler(DataBrowserController controller, DeviceTreePanel deviceTreePanel) {
        super(controller);
        tree = deviceTreePanel;
    }

    public TreeItemTransferHandler(final DataBrowserController controller, final DataType forcedType,
            final AxisType type, DeviceTreePanel deviceTreePanel) {
        super(controller, forcedType, type);
        tree = deviceTreePanel;
    }

    @Override
    protected boolean importData() {
        boolean importe = true;
        List<String> selectedDevices = tree.getSelectedDevices();
        TangoDeviceExplorerController tangoController = (TangoDeviceExplorerController) controller;
        if ((type == null) || (forcedType == null)) {
            tangoController.openItem(selectedDevices);
        } else {
            LOGGER.trace("openItem ({},{})", forcedType, type);
            tangoController.openItem(selectedDevices, forcedType, type);
        }
        return importe;
    }

    @Override
    protected boolean isDropEnable(DataType dataType) {
        boolean dropEnable = false;
        List<String> selectedDevices = tree.getSelectedDevices();
        if (selectedDevices != null) {
            TangoDeviceExplorerController tangoController = (TangoDeviceExplorerController) controller;
            for (String attribute : selectedDevices) {
                List<IKey> keysForAttributes = tangoController.getKeysForAttributes(attribute);
                if (keysForAttributes != null) {
                    for (IKey iKey : keysForAttributes) {
                        dropEnable = tangoController.isMatchingType(iKey, dataType);
                        if (dropEnable) {
                            break;
                        }
                    }// End for
                }// End if
                if (dropEnable) {
                    break;
                }
            }// End for
        }// End if
        return dropEnable;
    }

}
