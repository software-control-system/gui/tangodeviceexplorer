package fr.soleil.comete.bean.tangodeviceexplorer.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import org.cdma.gui.databrowser.DataBrowser;
import org.cdma.gui.databrowser.DataBrowserItemEvent;
import org.cdma.gui.databrowser.DataSourceManager;
import org.cdma.gui.databrowser.interfaces.AxisType;
import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.DataType;
import org.cdma.gui.databrowser.interfaces.IDataBrowserItemListener;
import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.model.DataItemViewer;
import org.cdma.gui.databrowser.view.item.ItemTableCellRenderer;
import org.jdesktop.swingx.JXTable;

import fr.soleil.comete.bean.tangodeviceexplorer.controller.TangoDeviceExplorerController;
import fr.soleil.comete.bean.tangodeviceexplorer.model.TangoAttributeSourceDevice;
import fr.soleil.comete.bean.tangotree.swing.SwingDeviceSelector;
import fr.soleil.comete.definition.widget.ITree;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.comete.swing.Tree;
import fr.soleil.comete.swing.util.ImageTool;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.swing.WindowSwingUtils;

public class DeviceTreePanel extends AbstractDeviceExplorerPanel implements MouseListener, IDataBrowserItemListener {

    private static final long serialVersionUID = -2047625591299634046L;

    private SwingDeviceSelector tangoSelector;
    private TangoDeviceExplorerController controller;

    private Tree tree;

    // PopMenu
    private JPopupMenu popupAttributeMenu;
    private JMenuItem menuOpenSource;
    private JMenuItem menuRemoveSource;
    private JMenuItem menuShowOnX;
    private JMenuItem menuSetXForOpenedSpectrum;
    private JMenuItem menuXMatrixItem;
    private JMenuItem menuYMatrixItem;
    private JMenuItem menuRemoveX;
    private JMenuItem menuOpenManySprectums;

    private JPopupMenu popupOpenAllSource;
    private JMenuItem menuOpenAllSources;
    private JMenuItem menuReload;

    private JPopupMenu popupCommandsMenu;
    private JMenuItem menuUseCommand;
    private JMenuItem menuAddCommandToCombo;

    private JDialog displayOpenItemDialog;
    private JDialog displayOpenManyItemDialog;

    private boolean spectrumMenu;
    private boolean putKeyOnX;

    public DeviceTreePanel(final TangoDeviceExplorerController controller) {
        this.controller = controller;
        controller.addDataBrowserItemListener(this);
        initComponent();
    }

    private void initComponent() {
        setLayout(new BorderLayout());

        tangoSelector = new SwingDeviceSelector(null, true, true);
        add((Component) tangoSelector.getSelectorComponent(), BorderLayout.CENTER);

        tree = (Tree) tangoSelector.getTree();
        tree.setDragEnabled(true);
        tree.addMouseListener(this);
        tree.setCellRenderer(new TangoDeviceExplorerTreeCellRenderer(controller));

        popupCommandsMenu = new JPopupMenu();
        menuUseCommand = new JMenuItem("Execute command");
        menuAddCommandToCombo = new JMenuItem("Add command to combobox");
        popupCommandsMenu.add(menuUseCommand);
        popupCommandsMenu.addSeparator();
        popupCommandsMenu.add(menuAddCommandToCombo);

        popupAttributeMenu = new JPopupMenu();

        menuOpenSource = new JMenuItem("Open data item");
        ImageIcon iconOpenData = TangoDeviceExplorerUtilities.ICONS.getIcon("TangoDeviceExplorer.OpenDataItem");
        menuOpenSource.setIcon(iconOpenData);

        menuRemoveSource = new JMenuItem("Remove data item");
        ImageIcon iconRemoveData = TangoDeviceExplorerUtilities.ICONS.getIcon("TangoDeviceExplorer.RemoveDataItem");
        menuRemoveSource.setIcon(iconRemoveData);

        menuShowOnX = new JMenuItem("Show on X");
        menuShowOnX.setIcon(ImageTool.getImage(TangoDeviceExplorerUtilities.SPECTRUM_NUM_IMAGE));

        menuSetXForOpenedSpectrum = new JMenuItem("Display on X for spectrum...");
        menuSetXForOpenedSpectrum.setIcon(ImageTool.getImage(TangoDeviceExplorerUtilities.SPECTRUM_NUM_IMAGE));

        menuXMatrixItem = new JMenuItem("Display on X scale for image...");
        menuXMatrixItem.setIcon(ImageTool.getImage(TangoDeviceExplorerUtilities.IMAGE_NUM_IMAGE));

        menuYMatrixItem = new JMenuItem("Display on Y scale for image...");
        menuYMatrixItem.setIcon(ImageTool.getImage(TangoDeviceExplorerUtilities.IMAGE_NUM_IMAGE));

        menuRemoveX = new JMenuItem("Remove X");

        menuOpenManySprectums = new JMenuItem("Open many spectrum");
        menuOpenManySprectums.setIcon(ImageTool.getImage(TangoDeviceExplorerUtilities.SPECTRUM_NUM_IMAGE));

        popupOpenAllSource = new JPopupMenu();
        menuOpenAllSources = new JMenuItem("Open all items");
        menuReload = new JMenuItem("Reload");
        menuReload.setIcon(DataBrowser.ICONS.getIcon("Action.ReloadSource"));

        popupOpenAllSource.add(menuOpenAllSources);
        popupOpenAllSource.add(menuReload);

        popupAttributeMenu.add(menuOpenSource);
        popupAttributeMenu.add(menuRemoveSource);
        popupAttributeMenu.addSeparator();
        popupAttributeMenu.add(menuOpenManySprectums);
        popupAttributeMenu.addSeparator();
        popupAttributeMenu.add(menuShowOnX);
        popupAttributeMenu.add(menuSetXForOpenedSpectrum);
        popupAttributeMenu.addSeparator();
        popupAttributeMenu.add(menuXMatrixItem);
        popupAttributeMenu.add(menuYMatrixItem);
        popupAttributeMenu.addSeparator();
        popupAttributeMenu.add(menuRemoveX);

        menuUseCommand.addActionListener((e) -> {
            executeCommandAction();
        });

        menuRemoveSource.addActionListener((e) -> {
            closeAllSelectedAttributes();
        });

        menuOpenSource.addActionListener((e) -> {
            openAttribute();
        });

        menuOpenManySprectums.addActionListener((e) -> {
            openManyItems();
        });

        menuShowOnX.addActionListener((e) -> {
            openOnX(DataType.SPECTRUM, AxisType.X);
        });

        menuSetXForOpenedSpectrum.addActionListener((e) -> {
            openDialogForOpenedSpectrumItems();
        });

        menuXMatrixItem.addActionListener((e) -> {
            openDialogXForOpenedMatrixItems();
        });

        menuYMatrixItem.addActionListener((e) -> {
            openDialogYForOpenedMatrixItems();
        });

        menuOpenAllSources.addActionListener((e) -> {
            openAllSources();
        });

        menuReload.addActionListener((e) -> {
            reloadSource();
        });

        menuAddCommandToCombo.addActionListener((e) -> {
            ITreeNode[] selectedNodes = tangoSelector.getTree().getSelectedNodes();
            for (ITreeNode treeNode : selectedNodes) {
                controller.addCommand(treeNode.getName());
            }
        });

        menuRemoveX.addActionListener((e) -> {
            Map<String, Item> openedAttribute = controller.getOpenedAttribute();
            for (String myAttribute : openedAttribute.keySet()) {
                Item item = openedAttribute.get(myAttribute);
                AxisType axis = item.getAxis();
                if (AxisType.X.equals(axis)) {
                    controller.closeItem(myAttribute);
                    break;
                }
            }
        });
    }

    protected void prepareAndShowDialog() {
        createSpectrumImageDialog();
        displayDialog();
    }

    private void openDialogForOpenedSpectrumItems() {
        spectrumMenu = true;
        putKeyOnX = true;
        prepareAndShowDialog();
    }

    private void openDialogXForOpenedMatrixItems() {
        spectrumMenu = false;
        putKeyOnX = true;
        prepareAndShowDialog();
    }

    private void openDialogYForOpenedMatrixItems() {
        spectrumMenu = false;
        putKeyOnX = false;
        prepareAndShowDialog();
    }

    private void createSpectrumImageDialog() {
        if (displayOpenItemDialog == null) {
            displayOpenItemDialog = new JDialog(WindowSwingUtils.getWindowForComponent(this));
            JPanel panel = new JPanel();
            panel.setLayout(new BorderLayout());
            final JXTable table = new JXTable();

            table.setSortable(true);
            table.setShowGrid(false);
            table.setShowHorizontalLines(true);
            table.setShowVerticalLines(true);
            table.setFillsViewportHeight(true);

            JScrollPane scrollPane = new JScrollPane(table);
            panel.add(scrollPane, BorderLayout.CENTER);

            JButton okButton = new JButton(
                    DataBrowser.MESSAGES.getString("DialogxAttributeForOpenAttributes.SourceTree.ButtonOk"));
            JButton cancelButton = new JButton(
                    DataBrowser.MESSAGES.getString("DialogxAttributeForOpenAttributes.SourceTree.ButtonCancel"));
            JPanel panelButton = new JPanel();
            panelButton.add(okButton);
            panelButton.add(cancelButton);

            panel.add(panelButton, BorderLayout.SOUTH);

            // Create table model
            TableModel model = new DefaultTableModel() {
                private static final long serialVersionUID = 5638421691050219305L;

                @Override
                public String getColumnName(int column) {
                    return DataBrowser.MESSAGES.getString("DialogxAttributeForOpenAttributes.SourceTree.HeaderTable");
                }

                @Override
                public int getColumnCount() {
                    return 1;
                }

                @Override
                public int getRowCount() {
                    int row = 0;
                    if (spectrumMenu) {
                        row = controller.getOpenedSpectrum().size();
                    } else {
                        row = controller.getOpenedImage().size();
                    }
                    return row;
                }

                @Override
                public Object getValueAt(int row, int column) {
                    Item item = null;
                    if (spectrumMenu) {
                        if (controller.getOpenedSpectrum() != null && row < controller.getOpenedSpectrum().size()) {
                            item = controller.getOpenedSpectrum().get(row);
                        }
                    } else {
                        if (controller.getOpenedImage() != null && row < controller.getOpenedImage().size()) {
                            item = controller.getOpenedImage().get(row);
                        }
                    }
                    return item;
                }
            };

            table.setModel(model);

            // Create renderer for Item
            ItemTableCellRenderer renderer = new ItemTableCellRenderer();
            TableColumn columnDate = table.getColumnModel().getColumn(0);
            columnDate.setCellRenderer(renderer);

            displayOpenItemDialog.setContentPane(panel);
            displayOpenItemDialog.setSize(600, 600);
            displayOpenItemDialog
                    .setTitle(DataBrowser.MESSAGES.getString("DialogxAttributeForOpenAttributes.SourceTree.Title"));
            displayOpenItemDialog.setAlwaysOnTop(true);
            displayOpenItemDialog.setModal(true);

            // Ok action
            okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    String xAttribut = null;
                    IKey xKey = null;

                    List<String> selectedattributs = getSelectedDevices();
                    if (selectedattributs != null && !selectedattributs.isEmpty()) {
                        for (String attributs : selectedattributs) {
                            xAttribut = attributs;
                        }
                    }

                    List<IKey> keysForAttributes = controller.getKeysForAttributes(xAttribut);
                    if (keysForAttributes != null && !keysForAttributes.isEmpty()) {
                        for (IKey key : keysForAttributes) {
                            xKey = key;
                        }
                    }

                    IDataSourceBrowser browser = DataSourceManager.getProducerFromKey(xKey);
                    DataType dataType = browser.getType(xKey);
                    DataFormat format = browser.getFormat(xKey);

                    int[] selectedRows = table.getSelectedRows();
                    Item item = null;
                    IKey sourceKey = null;
                    for (int index : selectedRows) {
                        item = (Item) table.getValueAt(index, 0);
                        sourceKey = TangoDeviceExplorerController.getSourceKey(xKey, item.getSourceKey());
                        if (putKeyOnX) {
                            item.setXKey(xKey, sourceKey, browser, dataType, format);
                        } else {
                            item.setXKey(xKey, sourceKey, browser, dataType, format);
                        }
                        controller.openItem(xAttribut, dataType, AxisType.X);
                        controller.setItemScale(item);
                    }
                    displayOpenItemDialog.dispose();
                }
            });

            cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    displayOpenItemDialog.dispose();
                }
            });
        }
    }

    private void displayDialog() {
        if (displayOpenItemDialog != null) {
            displayOpenItemDialog.setLocationRelativeTo(this);
            displayOpenItemDialog.setVisible(true);
            displayOpenItemDialog.toFront();
        }
    }

    private void reloadSource() {
        if (controller != null) {

            // Keep open attribute
            Map<String, Item> openedItem = new HashMap<>(controller.getOpenedAttribute());
            // Keep open command
            List<String> openedCommand = new ArrayList<>(controller.getOpenedCommand());

            controller.removeAllCommand();

            // Close all source if attributes in case of change of type
            closeAllOpenedAttributes();

            // setModel
            setModel(controller.getDeviceName());

            // Open command
            for (String command : openedCommand) {
                if (controller.containsCommand(command)) {
                    controller.addCommand(command);
                }
            }

            openedCommand.clear();

            // Open attribute
            Set<Entry<String, Item>> entrySet = openedItem.entrySet();
            String attributeName = null;
            Item item = null;
            for (Entry<String, Item> entry : entrySet) {
                attributeName = entry.getKey();
                item = entry.getValue();
                if (controller.containsAttribute(attributeName)) {
                    controller.openItem(attributeName, item.getType(), item.getAxis());
                }
            }

            openedItem.clear();
        }

    }

    private void executeCommandAction() {
        // System.out.println("executeCommandAction");
        ITreeNode[] selectedNodes = tangoSelector.getTree().getSelectedNodes();
        ITreeNode treeNode = selectedNodes[0];
        String commandName = treeNode.getName();
        controller.executeCommand(commandName);
    }

    @Override
    public void setModel(String model) {
        super.setModel(model);
        tangoSelector.setSourceDevice(controller.getTangoSourceDevice());
    }

    public void openAllSources() {
        TangoAttributeSourceDevice attributeSource = controller.getTangoSourceDevice().getAttributeSource();
        List<String> attributeList = attributeSource.getAttributeList();
        if (!attributeList.isEmpty() && attributeList != null) {
            controller.openItem(attributeList);
        }
    }

    public void openAllScalar() {
        List<String> allScalarList = controller.getAllScalarList();
        if (!allScalarList.isEmpty() && allScalarList != null) {
            controller.openItem(allScalarList);
        }
    }

    public void openAllSpectrum() {
        List<String> allSpectrumList = controller.getAllSpectrumList();
        if (!allSpectrumList.isEmpty() && allSpectrumList != null) {
            controller.openItem(allSpectrumList);
        }
    }

    public void openAllImage() {
        List<String> allImageList = controller.getAllImageList();
        if (!allImageList.isEmpty() && allImageList != null) {
            controller.openItem(allImageList);
        }
    }

    @Override
    protected void clearGUI() {
        // TODO Auto-generated method stub
    }

    @Override
    protected void refreshGUI() {
        // TODO Auto-generated method stub
    }

    public void openAttribute() {
        List<String> selectedDevices = getSelectedDevices();
        if (selectedDevices != null && !selectedDevices.isEmpty()) {
            controller.openItem(selectedDevices);
        }
    }

    private void openManyItems() {
        if (displayOpenManyItemDialog == null) {
            displayOpenManyItemDialog = new JDialog(WindowSwingUtils.getWindowForComponent(this));
            JPanel panel = new JPanel();
            panel.setLayout(new BorderLayout());
            final JXTable table = new JXTable();

            final List<String> allSpectrumImageAttributeList = new ArrayList<>();
            allSpectrumImageAttributeList.clear();

            table.setSortable(true);
            table.setShowGrid(false);
            table.setShowHorizontalLines(true);
            table.setShowVerticalLines(true);
            table.setFillsViewportHeight(true);

            JScrollPane scrollPane = new JScrollPane(table);
            panel.add(scrollPane, BorderLayout.CENTER);

            JButton okButton = new JButton(
                    DataBrowser.MESSAGES.getString("DialogxAttributeForOpenAttributes.SourceTree.ButtonOk"));
            JButton cancelButton = new JButton(
                    DataBrowser.MESSAGES.getString("DialogxAttributeForOpenAttributes.SourceTree.ButtonCancel"));
            JPanel panelButton = new JPanel();
            panelButton.add(okButton);
            panelButton.add(cancelButton);

            panel.add(panelButton, BorderLayout.SOUTH);

            // Create list of all attribute sprectrum
            TangoAttributeSourceDevice attributeSource = controller.getTangoSourceDevice().getAttributeSource();
            List<String> attributeList = attributeSource.getAttributeList();
            for (String attribute : attributeList) {
                List<IKey> keysForAttributes = controller.getKeysForAttributes(attribute);
                for (IKey key : keysForAttributes) {
                    DataType keyType = controller.getDataSourceBrowser().getKeyType(key);
                    DataFormat attributeFormat = controller.getTangoSourceDevice().getAttributeSource()
                            .getAttributeFormat(attribute);
                    if (DataType.SPECTRUM.equals(keyType)) {
                        if (DataFormat.BOOLEAN.equals(attributeFormat)
                                || DataFormat.NUMERICAL.equals(attributeFormat)) {
                            if (!allSpectrumImageAttributeList.contains(attribute)) {
                                allSpectrumImageAttributeList.add(attribute);
                            }
                        }
                    }
                }
            }

            // Create table model
            TableModel model = new DefaultTableModel() {
                private static final long serialVersionUID = 5638421691050219305L;

                @Override
                public String getColumnName(int column) {
                    return "Items";
                }

                @Override
                public int getColumnCount() {
                    return 1;
                }

                @Override
                public int getRowCount() {
                    int row = 0;
                    if ((allSpectrumImageAttributeList != null) && (!allSpectrumImageAttributeList.isEmpty())) {
                        row = allSpectrumImageAttributeList.size();
                    }
                    return row;
                }

                @Override
                public Object getValueAt(int row, int column) {
                    String attributeName = null;

                    if ((allSpectrumImageAttributeList != null) && (!allSpectrumImageAttributeList.isEmpty()
                            && row < allSpectrumImageAttributeList.size())) {
                        attributeName = allSpectrumImageAttributeList.get(row);
                    }
                    return attributeName;
                }
            };

            table.setModel(model);

            displayOpenManyItemDialog.setContentPane(panel);
            displayOpenManyItemDialog.setSize(600, 600);
            displayOpenManyItemDialog.setTitle("Select items to open");
            displayOpenManyItemDialog.setAlwaysOnTop(true);

            okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    final List<String> toOpenAttribute = new ArrayList<>();
                    List<String> selectedDevices = getSelectedDevices();
                    for (String selectedDevice : selectedDevices) {
                        toOpenAttribute.add(selectedDevice);
                    }

                    int[] selectedRows = table.getSelectedRows();
                    String attribute = null;
                    for (int index : selectedRows) {
                        attribute = (String) table.getValueAt(index, 0);
                        toOpenAttribute.add(attribute);
                        controller.getKeysForAttributes(attribute);
                    }

                    DataItemViewer dataItemViewer = controller.getDataItemViewer();
                    dataItemViewer.setOpenTabWithManyItem(true);
                    controller.openItem(toOpenAttribute);
                    dataItemViewer.setOpenTabWithManyItem(false);
                    dataItemViewer.setAllItemsIsInTab(true);
                    displayOpenManyItemDialog.dispose();
                }
            });

            cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    displayOpenManyItemDialog.dispose();
                }
            });
        }

        displayOpenManyItemDialog.setModal(true);
        displayOpenManyItemDialog.setLocationRelativeTo(this);
        displayOpenManyItemDialog.setVisible(true);
        displayOpenManyItemDialog.toFront();
    }

    public void openOnX(DataType forcedDataType, AxisType axisType) {
        List<String> selectedDevices = getSelectedDevices();
        if (selectedDevices != null && !selectedDevices.isEmpty() && forcedDataType != null && axisType != null) {
            controller.openItem(selectedDevices, forcedDataType, axisType);
        }
    }

    public List<String> getSelectedCommands() {
        List<String> selectedCommands = tangoSelector.getSelectedDevices();
        // System.out.println("selectedCommands " + selectedCommands);
        String[] split = null;
        List<String> selectedCmd = new ArrayList<>();
        for (String selected : selectedCommands) {
            split = selected.split(TangoDeviceExplorerUtilities.PATH_SEPARATOR);
            if (split[0].equals(TangoDeviceExplorerUtilities.COMMANDS)) {
                if (split != null && split.length > 0) {
                    selectedCmd.add(split[split.length - 1]);
                }
            }
        }
        return selectedCmd;
    }

    public List<String> getSelectedDevices() {
        List<String> selectedDevices = tangoSelector.getSelectedDevices();
        // System.out.println("selectedDevices " + selectedDevices);
        List<String> selectedDev = null;
        if (selectedDevices != null) {
            selectedDev = new ArrayList<>();
            String[] split = null;
            for (String selected : selectedDevices) {
                split = selected.split(TangoDeviceExplorerUtilities.PATH_SEPARATOR);
                if (split != null && split.length > 0) {
                    selectedDev.add(split[split.length - 1]);
                }
            }
        }
        return selectedDev;
    }

    private void closeAllOpenedAttributes() {
        controller.closeAllOpenItem();
    }

    private void closeAllSelectedAttributes() {
        ITree tree = tangoSelector.getTree();
        if (tree != null) {
            ITreeNode[] selectedNodes = tree.getSelectedNodes();
            for (ITreeNode treeNode : selectedNodes) {
                String name = treeNode.getName();
                controller.closeItem(name);
            }
        }
    }

    private void onPopupTrigger(final MouseEvent e) {
        if (e.isPopupTrigger()) {
            Tree tree = (Tree) tangoSelector.getTree();
            String parentName = null;
            int pointedRow = tree.getRowForLocation(e.getX(), e.getY());
            if (pointedRow != -1) {
                ITreeNode[] selectedNodes = tree.getSelectedNodes();
                if (selectedNodes != null) {
                    ITreeNode selectedNode = selectedNodes[0];
                    if (selectedNode.getName() == controller.getDeviceName()) {
                        popupOpenAllSource.show(e.getComponent(), e.getX(), e.getY());
                    } else {
                        if (selectedNode.getParent() != null) {
                            parentName = selectedNode.getParent().getName();
                            if (!parentName.equalsIgnoreCase(controller.getDeviceName())) {
                                if (parentName.equals(TangoDeviceExplorerUtilities.COMMANDS)) {
                                    popupCommandsMenu.show(e.getComponent(), e.getX(), e.getY());
                                } else {
                                    List<Item> openedAttribute = controller.getOpenedSpectrumImageAttribute();
                                    for (Item item : openedAttribute) {
                                        AxisType axis = item.getAxis();
                                        if (AxisType.X.equals(axis)) {
                                            menuRemoveX.setEnabled(true);
                                        } else {
                                            menuRemoveX.setEnabled(false);
                                        }
                                    }
                                    if ((tree.getSelectedNodes().length == 1)
                                            && (controller.getOpenedSpectrum() != null)
                                            && (!controller.getOpenedSpectrum().isEmpty())
                                            && (!parentName.equals(DataType.SCALAR.toString()))) {
                                        menuShowOnX.setEnabled(true);
                                        menuSetXForOpenedSpectrum.setEnabled(true);
                                    } else {
                                        menuRemoveX.setEnabled(false);
                                        menuShowOnX.setEnabled(false);
                                        menuSetXForOpenedSpectrum.setEnabled(false);
                                    }
                                    if ((tree.getSelectedNodes().length == 1) && (controller.getOpenedImage() != null)
                                            && (!controller.getOpenedImage().isEmpty())
                                            && (!parentName.equals(DataType.SCALAR.toString()))) {
                                        menuXMatrixItem.setEnabled(true);
                                        menuYMatrixItem.setEnabled(true);
                                    } else {
                                        menuXMatrixItem.setEnabled(false);
                                        menuYMatrixItem.setEnabled(false);
                                    }
                                    boolean itemOpened = controller.isItemOpened(selectedNode.getName());
                                    if (itemOpened) {
                                        menuOpenSource.setEnabled(false);
                                        menuRemoveSource.setEnabled(true);
                                    } else {
                                        menuOpenSource.setEnabled(true);
                                        menuRemoveSource.setEnabled(false);
                                    }
                                    popupAttributeMenu.show(e.getComponent(), e.getX(), e.getY());
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        List<String> selectedDevices = getSelectedDevices();
        boolean isAttribute = false;
        if (selectedDevices != null && !selectedDevices.isEmpty()) {
            String attributeName = null;
            for (String device : selectedDevices) {
                // Set selected Item if it is an attribute
                isAttribute = controller.getTangoSourceDevice().getAttributeSource().contains(device);
                boolean isOpened = controller.isItemOpened(device);
                if (isAttribute && isOpened) {
                    attributeName = device;
                    break;
                }
            }
            if (attributeName != null) {
                List<IKey> keysForAttributes = controller.getKeysForAttributes(attributeName);
                if (keysForAttributes != null && !keysForAttributes.isEmpty()) {
                    controller.selectItems(keysForAttributes.get(0));
                }
            }
        }
        if ((e.getClickCount() == 2) && SwingUtilities.isLeftMouseButton(e)) {
            if (isAttribute) {
                openAttribute();
            } else if (getSelectedCommands() != null) {
                List<String> selectedCommands = getSelectedCommands();
                if (!selectedCommands.isEmpty()) {
                    executeCommandAction();
                }
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        onPopupTrigger(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        onPopupTrigger(e);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // Useless
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // Useless
    }

    @Override
    public void onItemOpened(DataBrowserItemEvent event) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onItemClosed(DataBrowserItemEvent event) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onItemSelected(DataBrowserItemEvent event) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onItemDeselected(DataBrowserItemEvent event) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onItemAxisChanged(DataBrowserItemEvent event) {
        // TODO Auto-generated method stub
    }
}
