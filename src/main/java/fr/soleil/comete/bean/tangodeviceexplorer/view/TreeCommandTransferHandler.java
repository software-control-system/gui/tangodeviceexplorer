package fr.soleil.comete.bean.tangodeviceexplorer.view;

import java.util.List;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.DataType;
import org.cdma.gui.databrowser.util.ItemTransferHandler;

import fr.soleil.comete.bean.tangodeviceexplorer.controller.TangoDeviceExplorerController;

public class TreeCommandTransferHandler extends ItemTransferHandler {

    private static final long serialVersionUID = -7850392434215551635L;
    private DeviceTreePanel tree;
    

    public TreeCommandTransferHandler(DataBrowserController controller, DeviceTreePanel deviceTreePanel,
            DeviceStatePanel deviceStatePanel) {
        super(controller);
        tree = deviceTreePanel;
    }

    @Override
    protected boolean importData() {
        boolean importe = true;
        List<String> selectedCommands = tree.getSelectedCommands();
        for (String comd : selectedCommands) {
            addElementToCombobox(comd);
        }
        return importe;
    }

    public void addElementToCombobox(String command) {
        if(controller instanceof TangoDeviceExplorerController){
            ((TangoDeviceExplorerController)controller).addCommand(command);
        }
    }

    @Override
    protected boolean isDropEnable(DataType dataType) {
        boolean dropEnable = false;
        if (dataType.equals(DataType.SCALAR)) {
            dropEnable = true;
        }
        return dropEnable;
    }

}
