package fr.soleil.comete.bean.tangodeviceexplorer.view;

import java.util.prefs.Preferences;

import fr.soleil.comete.box.AbstractTangoBox;

public abstract class AbstractDeviceExplorerPanel extends AbstractTangoBox {

    private static final long serialVersionUID = -5878977558085735778L;
 
    @Override
    protected void onConnectionError() {
    }

    @Override
    protected void savePreferences(Preferences preferences) {
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
    }

}
