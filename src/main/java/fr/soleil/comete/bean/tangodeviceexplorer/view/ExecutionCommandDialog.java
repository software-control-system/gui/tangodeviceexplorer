package fr.soleil.comete.bean.tangodeviceexplorer.view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.jdesktop.swingx.JXErrorPane;

import fr.soleil.comete.bean.tangodeviceexplorer.controller.TangoDeviceExplorerController;
import fr.soleil.comete.bean.tangodeviceexplorer.model.TangoCommandSourceDevice;
import fr.soleil.comete.swing.TextArea;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.tango.clientapi.TangoCommand;

public class ExecutionCommandDialog extends JDialog {

    private static final long serialVersionUID = -2404346289874980566L;

    private JPanel mainPanel;

    private JPanel northPanel;
    private JScrollPane centerPanel;
    private JPanel southPanel;

    private JTextArea textArea;
    private JButton buttonExecute;

    private TextArea textField;

    private JButton buttonClose;
    private JLabel helpButton;
    private String command;

    private AdvancedStringMatrixArea spectrumPanel;
    private AdvancedStringMatrixArea stringMixedPanel;
    private AdvancedStringMatrixArea numberMixedPanel;
    private JPanel mixedPanel;
    private CardLayout cardLayout;

    private final TangoDeviceExplorerController controller;

    public ExecutionCommandDialog(Window parent, TangoDeviceExplorerController controller) {
        super(parent);
        this.controller = controller;
        initComponent();
    }

    private void initComponent() {
        mainPanel = new JPanel(new BorderLayout());

        centerPanel = new JScrollPane();
        southPanel = new JPanel();

        textField = new TextArea();
        buttonExecute = new JButton("Execute");

        textArea = new JTextArea();
        textArea.setBackground(Color.LIGHT_GRAY);
        textArea.setEditable(false);

        buttonClose = new JButton("Close");
        helpButton = new JLabel(" ? ");
        helpButton.setBorder(BorderFactory.createLoweredBevelBorder());

        cardLayout = new CardLayout();

        northPanel = new JPanel(cardLayout);
        northPanel.add(textField, LAYOUT.scalar.toString());

        spectrumPanel = new AdvancedStringMatrixArea();

        northPanel.add(spectrumPanel, LAYOUT.spectrum.toString());

        mixedPanel = new JPanel(new BorderLayout());
        JPanel numberPanel = new JPanel(new BorderLayout());
        numberMixedPanel = new AdvancedStringMatrixArea();
        numberPanel.add(new JLabel("\tValue"), BorderLayout.NORTH);
        numberPanel.add(numberMixedPanel, BorderLayout.CENTER);

        JPanel stringPanel = new JPanel(new BorderLayout());
        stringMixedPanel = new AdvancedStringMatrixArea();
        stringPanel.add(new JLabel("\tString"), BorderLayout.NORTH);
        stringPanel.add(stringMixedPanel, BorderLayout.CENTER);

        mixedPanel.add(numberPanel, BorderLayout.WEST);
        mixedPanel.add(stringPanel, BorderLayout.EAST);
        northPanel.add(mixedPanel, LAYOUT.mixed.toString());

        northPanel.add(new JLabel("Result"), LAYOUT.result.toString());

        centerPanel.setViewportView(textArea);

        southPanel.add(buttonClose);
        southPanel.add(buttonExecute);
        southPanel.add(helpButton);

        mainPanel.add(northPanel, BorderLayout.NORTH);
        mainPanel.add(centerPanel, BorderLayout.CENTER);
        mainPanel.add(southPanel, BorderLayout.SOUTH);

        add(mainPanel);

        buttonExecute.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.setText(ObjectUtils.EMPTY_STRING);

                Object argin = null;
                if (textField.isShowing()) {
                    argin = textField.getText();
                } else if (spectrumPanel.isShowing()) {
                    spectrumPanel.updateValues();
                    argin = spectrumPanel.getFlatStringMatrix();
                } else if (stringMixedPanel.isShowing() && numberMixedPanel.isShowing()) {
                    stringMixedPanel.updateValues();
                    numberMixedPanel.updateValues();
                    Object[] arginArray = new Object[2];
                    arginArray[0] = stringMixedPanel.getFlatStringMatrix();
                    arginArray[1] = numberMixedPanel.getFlatStringMatrix();
                    argin = arginArray;
                }

                TangoCommandSourceDevice tangoCommandSourceDevice = controller.getTangoSourceDevice()
                        .getCommandSource();
                Object executeCommand = tangoCommandSourceDevice.executeCommand(command, argin);
                if (executeCommand instanceof Exception) {
                    JXErrorPane.showDialog((Exception) executeCommand);
                } else if (executeCommand != null) {
                    textArea.setText(String.valueOf(executeCommand));
                }
            }
        });

        buttonClose.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.setText(ObjectUtils.EMPTY_STRING);
                dispose();
            }
        });

    }

    public void show(String command) {
        textField.setText(ObjectUtils.EMPTY_STRING);
        textArea.setText(ObjectUtils.EMPTY_STRING);
        spectrumPanel.setText(ObjectUtils.EMPTY_STRING);
        spectrumPanel.updateValues();
        stringMixedPanel.setText(ObjectUtils.EMPTY_STRING);
        stringMixedPanel.updateValues();
        numberMixedPanel.setText(ObjectUtils.EMPTY_STRING);
        numberMixedPanel.updateValues();

        helpButton.setToolTipText(controller.getTangoSourceDevice().getCommandSource().getCommandDescription(command));

        this.command = command;
        this.setTitle(controller.getDeviceName() + "/" + command);
        TangoCommand tangoCommand = controller.getCommandArginType(command);
        if (tangoCommand != null) {
            try {
                if (tangoCommand.isArginScalar()) {
                    cardLayout.show(northPanel, LAYOUT.scalar.toString());
                } else if (tangoCommand.isArginSpectrum()) {
                    cardLayout.show(northPanel, LAYOUT.spectrum.toString());
                } else if (tangoCommand.isArginMixFormat()) {
                    cardLayout.show(northPanel, LAYOUT.mixed.toString());
                } else {
                    cardLayout.show(northPanel, LAYOUT.scalar.toString());
                }
            } catch (Exception e) {

            }
        } else {
            cardLayout.show(northPanel, LAYOUT.scalar.toString());
        }
        this.setVisible(true);
        this.toFront();
    }

    public void showResult(String command, String result) {
        textField.setText(ObjectUtils.EMPTY_STRING);
        textArea.setText(ObjectUtils.EMPTY_STRING);
        spectrumPanel.setText(ObjectUtils.EMPTY_STRING);
        spectrumPanel.updateValues();
        stringMixedPanel.setText(ObjectUtils.EMPTY_STRING);
        stringMixedPanel.updateValues();
        numberMixedPanel.setText(ObjectUtils.EMPTY_STRING);
        numberMixedPanel.updateValues();
        helpButton.setToolTipText(controller.getTangoSourceDevice().getCommandSource().getCommandDescription(command));
        this.setVisible(true);
        this.toFront();

        this.command = command;
        this.setTitle(controller.getDeviceName() + "/" + command);
        cardLayout.show(northPanel, LAYOUT.result.toString());

        textArea.setText(result);
    }

    public JTextArea getTextArea() {
        return textArea;
    }

    public static void main(String[] args) {
        ExecutionCommandDialog dialogCommands = new ExecutionCommandDialog(null,
                new TangoDeviceExplorerController(null));
        dialogCommands.setVisible(true);
        dialogCommands.toFront();
        dialogCommands.setLocationRelativeTo(null);
        dialogCommands.setSize(450, 350);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private enum LAYOUT {
        scalar, spectrum, mixed, result
    };

}
