/*******************************************************************************
 * Copyright (c) 2013-2014 Synchrotron SOLEIL.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 ******************************************************************************/
package fr.soleil.comete.bean.tangodeviceexplorer.view;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTree;

import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.DataType;

import fr.soleil.comete.bean.tangodeviceexplorer.controller.TangoDeviceExplorerController;
import fr.soleil.comete.bean.tangodeviceexplorer.model.TangoCommandSourceDevice;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.comete.swing.util.CometeTreeCellRenderer;
import fr.soleil.comete.swing.util.ImageTool;

public class TangoDeviceExplorerTreeCellRenderer extends CometeTreeCellRenderer {

    private static final long serialVersionUID = -5316796510830475912L;

    private TangoDeviceExplorerController controller;

    public TangoDeviceExplorerTreeCellRenderer(TangoDeviceExplorerController controller) {
        super();
        this.controller = controller;
    }

    @Override
    public Component getTreeCellRendererComponent(final JTree tree, final Object value, final boolean sel,
            final boolean expanded, final boolean leaf, final int row, final boolean hasTheFocus) {
        Component superComponent = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row,
                hasTheFocus);
        ITreeNode treeNode = getTreeNode(value, tree);
        if (superComponent instanceof JLabel && treeNode != null) {
            // Manage parent node
            JLabel label = (JLabel) superComponent;
            // Manage command or attribute
            String valueString = treeNode.getName();
            // System.out.println("valueString=" + valueString);
            if (leaf) {
                if (controller.isItemOpened(valueString)) {
                    label.setForeground(Color.BLUE);
                }
                // Manage tool tip
                String toolTip = controller.getTangoSourceDevice().getCommandSource()
                        .getCommandDescription(valueString);
                if (toolTip == null) {
                    toolTip = controller.getTangoSourceDevice().getAttributeSource()
                            .getAttributeDescription(valueString);
                }
                if (toolTip != null) {
                    label.setToolTipText(toolTip);
                }

                // Manage Icon
                if (treeNode.getParent() != null) {
                    String parentName = treeNode.getParent().getName();
                    setIcon(label, parentName, valueString);
                }
            } else {
                setIcon(label, valueString);
            }
        }
        return superComponent;
    }

    private void setIcon(final JLabel label, final String valueType) {
        setIcon(label, valueType, null);
    }

    private void setIcon(final JLabel label, final String valueType, String nodeName) {
        if (DataType.IMAGE.toString().equals(valueType)) {
            if (nodeName != null) {
                DataFormat attributeFormat = controller.getTangoSourceDevice().getAttributeSource()
                        .getAttributeFormat(nodeName);
                switch (attributeFormat) {
                    case VOID:
                    case TEXT:
                        label.setIcon(ImageTool.getImage(TangoDeviceExplorerUtilities.IMAGE_TEXT_IMAGE));
                        break;
                    case BOOLEAN:
                    case NUMERICAL:
                        label.setIcon(ImageTool.getImage(TangoDeviceExplorerUtilities.IMAGE_NUM_IMAGE));
                        break;
                }
            } else {
                label.setIcon(ImageTool.getImage(TangoDeviceExplorerUtilities.IMAGE_NUM_IMAGE));
            }
        } else if (DataType.SPECTRUM.toString().equals(valueType)) {
            if (nodeName != null) {
                DataFormat attributeFormat = controller.getTangoSourceDevice().getAttributeSource()
                        .getAttributeFormat(nodeName);
                switch (attributeFormat) {
                    case VOID:
                    case TEXT:
                        label.setIcon(ImageTool.getImage(TangoDeviceExplorerUtilities.SPECTRUM_TEXT_IMAGE));
                        break;
                    case BOOLEAN:
                    case NUMERICAL:
                        label.setIcon(ImageTool.getImage(TangoDeviceExplorerUtilities.SPECTRUM_NUM_IMAGE));
                        break;
                }
            } else {
                label.setIcon(ImageTool.getImage(TangoDeviceExplorerUtilities.SPECTRUM_NUM_IMAGE));
            }
        } else if (DataType.SCALAR.toString().equals(valueType)) {
            label.setIcon(ImageTool.getImage(TangoDeviceExplorerUtilities.SCALAR_IMAGE));
        } else if (TangoCommandSourceDevice.COMMANDS_NODE.equals(valueType)) {
            label.setIcon(ImageTool.getImage(TangoDeviceExplorerUtilities.COMMANDS_IMAGE));
        }
    }

}
