package fr.soleil.comete.bean.tangodeviceexplorer.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.soleil.comete.bean.tangodeviceexplorer.controller.TangoDeviceExplorerController;

public abstract class AbstractTangoDeviceSource {

    protected static Map<String, String> descriptionMap = new HashMap<String, String>();
    protected boolean expertLevel = false;
    protected final TangoDeviceExplorerController controller;
    
    public AbstractTangoDeviceSource(final TangoDeviceExplorerController controller) {
        this.controller = controller;
    }
    
    public void setExpertLevel(boolean expertLevel){
        this.expertLevel = expertLevel;
        //TODO reload 
    }

    protected String getDeviceName() {
        return controller.getDeviceName();
    }
    
    public abstract void createSourceList();
    
    public abstract List<String> getDataSourceList();
    
    public abstract boolean contains(final String key);

}
