package fr.soleil.comete.bean.tangodeviceexplorer.view;

import java.util.ResourceBundle;

import org.cdma.gui.databrowser.DataBrowser;

import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.lib.project.swing.icons.Icons;

public class TangoDeviceExplorerUtilities {

    public final static Icons ICONS = new Icons("fr.soleil.comete.bean.tangodeviceexplorer.icons");

    public static final String PATH_SEPARATOR = ":";

    public static final String COMMAND_INIT = "Init";
    public static final String COMMAND_STATE = "State";
    public static final String COMMAND_Status = "Status";
    public static final String COMMANDS = "COMMANDS"; 

    private static final ResourceBundle ICON = ResourceBundle.getBundle(DataBrowser.ICONS_PATH);
    public static CometeImage SCALAR_IMAGE = null;
    public static CometeImage SPECTRUM_NUM_IMAGE = null;
    public static CometeImage SPECTRUM_TEXT_IMAGE = null;
    public static CometeImage SPECTRUM_IMAGE = null;
    public static CometeImage IMAGE_NUM_IMAGE = null;
    public static CometeImage IMAGE_TEXT_IMAGE = null;
    public static CometeImage IMAGE_IMAGE = null;
    public static CometeImage COMMANDS_IMAGE = null; 

    static {
        try {
            String iconKey = ICON.getString("SourceTree.Node.Browser");

            iconKey = ICON.getString("SourceTree.Node.Item.Scalar");
            SCALAR_IMAGE = new CometeImage(TangoDeviceExplorer.class.getResource(iconKey).toURI());

            iconKey = ICON.getString("SourceTree.Node.Item.Spectrum.Numerical");
            SPECTRUM_NUM_IMAGE = new CometeImage(TangoDeviceExplorer.class.getResource(iconKey).toURI());
            
            iconKey = ICON.getString("SourceTree.Node.Item.Spectrum");
            SPECTRUM_TEXT_IMAGE = new CometeImage(TangoDeviceExplorer.class.getResource(iconKey).toURI());

            iconKey = ICON.getString("SourceTree.Node.Item.Spectrum");
            SPECTRUM_IMAGE = new CometeImage(TangoDeviceExplorer.class.getResource(iconKey).toURI());

            iconKey = ICON.getString("SourceTree.Node.Item.Image.Numerical");
            IMAGE_NUM_IMAGE = new CometeImage(TangoDeviceExplorer.class.getResource(iconKey).toURI());
            
            iconKey = ICON.getString("SourceTree.Node.Item.Image");
            IMAGE_TEXT_IMAGE = new CometeImage(TangoDeviceExplorer.class.getResource(iconKey).toURI());

            iconKey = ICON.getString("SourceTree.Node.Item.Image");
            IMAGE_IMAGE = new CometeImage(TangoDeviceExplorer.class.getResource(iconKey).toURI());
            
            iconKey = ICON.getString("SourceTree.Node.Item.Commands");
            COMMANDS_IMAGE = new CometeImage(TangoDeviceExplorer.class.getResource(iconKey).toURI());

        } catch (Exception e) {
            // e.printStackTrace();
        }
    }
}
