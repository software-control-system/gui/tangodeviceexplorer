package fr.soleil.comete.bean.tangodeviceexplorer.view;

import fr.soleil.comete.swing.StringMatrixArea;

public class AdvancedStringMatrixArea extends StringMatrixArea {

    /**
     * 
     */
    private static final long serialVersionUID = -8033639094646054686L;
    
    public AdvancedStringMatrixArea() {
        super();
        getWriteButton().setVisible(false);
        //getUpdateButton().setVisible(false);
        getCancelButton().setVisible(false);
    }
    
    public void updateValues(){
        updateValue();
    }

}
