package fr.soleil.comete.bean.tangodeviceexplorer.controller;

import java.awt.Cursor;
import java.awt.Frame;
import java.awt.Window;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

import javax.swing.SwingWorker;

import org.cdma.gui.databrowser.DataBrowser;
import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.DataBrowserItemEvent;
import org.cdma.gui.databrowser.DataSourceManager;
import org.cdma.gui.databrowser.KeyParameter;
import org.cdma.gui.databrowser.LoadParameter;
import org.cdma.gui.databrowser.exception.LoadException;
import org.cdma.gui.databrowser.impl.tango.TangoDataSourceBrowser;
import org.cdma.gui.databrowser.interfaces.AxisType;
import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.DataType;
import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.model.DataItemViewer;
import org.cdma.gui.databrowser.util.DockingUtil;
import org.jdesktop.swingx.JXErrorPane;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.DOMBuilder;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.DOMOutputter;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.tangodeviceexplorer.model.TangoSourceDevice;
import fr.soleil.comete.bean.tangodeviceexplorer.view.DeviceStatePanel;
import fr.soleil.comete.bean.tangodeviceexplorer.view.ExecutionCommandDialog;
import fr.soleil.comete.bean.tangodeviceexplorer.view.TangoDeviceExplorer;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.xml.ChartPropertiesXmlManager;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.data.exception.KeyCompatibilityException;
import fr.soleil.data.manager.SimpleRandomTaskManager;
import fr.soleil.data.service.AbstractKey;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.IKey;
import fr.soleil.data.service.PolledRefreshingStrategy;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.lib.project.swing.dialog.ProgressDialog;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;
import fr.soleil.lib.project.xmlhelpers.exception.XMLWarning;
import fr.soleil.tango.clientapi.TangoCommand;

public class TangoDeviceExplorerController extends DataBrowserController {

    protected static final SimpleRandomTaskManager ITEM_SCHEDULER = new SimpleRandomTaskManager(
            LoggerFactory.getLogger(DataItemViewer.class), "TangoDeviceExplorerController item scheduler");

    private static final Logger LOGGER = LoggerFactory.getLogger(TangoDeviceExplorer.class);
    private static final ConcurrentMap<Integer, PolledRefreshingStrategy> REFRESHING_MAP = new ConcurrentHashMap<>();

    private final TangoDataSourceBrowser dataSourceBrowser;
    private String deviceName;
    private IKey knownSourceKey;
    private Map<String, Item> openedAttribute;
    private List<Item> openedSpectrumImageAttribute;
    private List<Item> openedSpectrum;
    private List<Item> openedImage;
    private TangoSourceDevice sourceDevice;
    private ExecutionCommandDialog executionCommandDialog;

    private TangoDeviceExplorer tangoDeviceExplorer;
    private ProgressDialog progressDialog;

    public TangoDeviceExplorerController(TangoDeviceExplorer tangoDeviceExplorer) {
        super();
        this.tangoDeviceExplorer = tangoDeviceExplorer;
        dataSourceBrowser = new TangoDataSourceBrowser();
        openedSourcesFromBrowser.put(dataSourceBrowser, new ArrayList<>());
        openedAttribute = new HashMap<>();
        sourceDevice = new TangoSourceDevice(this);
    }

    protected ProgressDialog getProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(WindowSwingUtils.getWindowForComponent(tangoDeviceExplorer));
        }
        return progressDialog;
    }

    public boolean containsCommand(final String command) {
        boolean contains = false;
        if (sourceDevice != null) {
            contains = sourceDevice.containsCommand(command);
        }
        return contains;
    }

    public boolean containsAttribute(final String attribute) {
        boolean contains = false;
        if (sourceDevice != null) {
            contains = sourceDevice.containsAttribute(attribute);
        }
        return contains;
    }

    public TangoSourceDevice getTangoSourceDevice() {
        return sourceDevice;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
        knownSourceKey = new TangoKey();
        TangoKeyTool.registerDeviceName(knownSourceKey, deviceName);
    }

    public DataItemViewer getDataItemViewer() {
        return tangoDeviceExplorer.getDataItemViewer();
    }

    public DeviceStatePanel getDeviceStatePanel() {
        return tangoDeviceExplorer.getDeviceStatePanel();
    }

    public String getDeviceName() {
        return deviceName;
    }

    public TangoDataSourceBrowser getDataSourceBrowser() {
        return dataSourceBrowser;
    }

    public void setRefreshingPeriod(int refreshingPeriod) {
        if (refreshingPeriod > 0) {
            PolledRefreshingStrategy polledRefreshingStrategy = REFRESHING_MAP.get(refreshingPeriod);
            if (polledRefreshingStrategy == null) {
                polledRefreshingStrategy = new PolledRefreshingStrategy(refreshingPeriod);
                PolledRefreshingStrategy tmp = REFRESHING_MAP.putIfAbsent(refreshingPeriod, polledRefreshingStrategy);
                if (tmp != null) {
                    polledRefreshingStrategy = tmp;
                }
            }
            Collection<Item> values = openedAttribute.values();
            IKey key = null;
            for (Item item : values) {
                key = item.getKey();
                DataSourceProducerProvider.setRefreshingStrategy(key, polledRefreshingStrategy);
            }
        }
    }

    public void stopRefreshingPeriod() {
        Collection<Item> values = openedAttribute.values();
        IKey key = null;
        for (Item item : values) {
            key = item.getKey();
            DataSourceProducerProvider.setRefreshingStrategy(key, null);

        }
    }

    public void setDeviceTimeOut(int parseInt) {
        TangoDeviceHelper.setTimeOutInMilliseconds(deviceName, parseInt);
    }

    public void setExpert(boolean expert) {
        getTangoSourceDevice().setExpertLevel(expert);
    }

    public DeviceProxy getDeviceProxy() {
        return TangoDeviceHelper.getDeviceProxy(deviceName);
    }

    // Save configuration file
    @Override
    public void exportViewConfiguration(final String filename) {
        if (filename != null) {

            Element racine = new Element("DevicePanelConfig");

            Document document = new Document(racine);

            DOMBuilder builder = new DOMBuilder();
            DataItemViewer dataItemViewer = getDataItemViewer();

            try {
                ChartProperties chartProperties = dataItemViewer.getChartProperties();
                if (chartProperties != null) {
                    String chartPropXML = ChartPropertiesXmlManager.toXmlString(chartProperties);
                    org.w3c.dom.Element chartPropNode;
                    chartPropNode = (org.w3c.dom.Element) XMLUtils.getRootNodeFromFileContent(chartPropXML);
                    Element elemChartProp = builder.build(chartPropNode);
                    elemChartProp.detach();
                    racine.addContent(elemChartProp);
                }
            } catch (XMLWarning e) {
                LOGGER.error("Impossible to export view configuration {}", e.getMessage());
                LOGGER.debug("Stack trace", e);
            }

            // XXX no more global scale item
//            // image scales
//            Item xScaleItem = dataItemViewer.getImageXScaleItem();
//            Item yScaleItem = dataItemViewer.getImageYScaleItem();
//            IKey xScaleItemKey = (xScaleItem == null ? null : xScaleItem.getKey());
//            IKey yScaleItemKey = (yScaleItem == null ? null : yScaleItem.getKey());
//
//            if (!openedAttribute.isEmpty()) {
//                Collection<Item> values = openedAttribute.values();
//                Element elemItems = new Element("items");
//                racine.addContent(elemItems);
//                IKey itemKey = null;
//                for (Item attributeItem : values) {
//                    itemKey = attributeItem.getKey();
//                    Element elemItem = new Element("item");
//                    elemItems.addContent(elemItem);
//
//                    Item theItem = items.get(itemKey);
//                    elemItem.setAttribute("axis", theItem.getAxis().name());
//
//                    if (itemKey == xScaleItemKey) {
//                        elemItem.setAttribute("imageXScale", "true");
//                    }
//                    if (itemKey == yScaleItemKey) {
//                        elemItem.setAttribute("imageYScale", "true");
//                    }
//
//                    try {
//                        // set item key
//                        String itemKeyXML = itemKey.toXML();
//                        org.w3c.dom.Element itemKeyNode = (org.w3c.dom.Element) XMLUtils
//                                .getRootNodeFromFileContent(itemKeyXML);
//                        Element elemItemKey = builder.build(itemKeyNode);
//                        elemItemKey.detach();
//                        elemItem.addContent(elemItemKey);
//
//                        // set item properties (plot or image)
//                        switch (theItem.getAxis()) {
//                            case Y1:
//                            case Y2:
//                            case Y1_COLUMN:
//                            case Y1_ROW:
//                            case Y2_COLUMN:
//                            case Y2_ROW: {
//                                String id = itemKey.getInformationKey();
//                                PlotProperties plotProperties = dataItemViewer.getChartDataViewPlotProperties(id);
//                                if (plotProperties != null) {
//                                    String plotPropXML = PlotPropertiesXmlManager.toXmlString(plotProperties);
//                                    org.w3c.dom.Element plotPropNode = (org.w3c.dom.Element) XMLUtils
//                                            .getRootNodeFromFileContent(plotPropXML);
//                                    Element elemPlotProp = builder.build(plotPropNode);
//                                    elemPlotProp.detach();
//                                    elemItem.addContent(elemPlotProp);
//                                }
//                            }
//                                break;
//
//                            case IMAGE: {
//                                ImageProperties imageProperties = dataItemViewer.getImageProperties(itemKey);
//                                if (imageProperties != null) {
//                                    String imagePropXML = ImagePropertiesXmlManager.toXmlString(imageProperties);
//
//                                    org.w3c.dom.Element imagePropNode;
//
//                                    imagePropNode = (org.w3c.dom.Element) XMLUtils
//                                            .getRootNodeFromFileContent(imagePropXML);
//                                    Element elemImageProp = builder.build(imagePropNode);
//                                    elemImageProp.detach();
//                                    elemItem.addContent(elemImageProp);
//                                }
//                            }
//                                break;
//
//                            default:
//                                break;
//                        }
//                    }
//
//                    catch (XMLWarning e) {
//                        e.printStackTrace();
//                    }
//
//                }
//            }

            // Put deviceName in XML
            Element elemDevice = new Element("device");
            racine.addContent(elemDevice);

            Element elemDeviceName = new Element("devicename");
            elemDevice.addContent(elemDeviceName);
            elemDeviceName.setAttribute("value", getDeviceName());

            Element elemDocking = new Element("docking");
            racine.addContent(elemDocking);

            // For DataItemViewer
            Element elemLayout = new Element("dataitemlayout");
            elemDocking.addContent(elemLayout);
            byte[] dataItemDocking = getDockingUtil().getLayout(DockingUtil.ITEM_VIEW_ID,
                    dataItemViewer.getDockingArea());

            String valueDataItemLayout = byteArrayToString(dataItemDocking);
            elemLayout.setAttribute("value", valueDataItemLayout);

            FileOutputStream fileOutputStream = null;
            try {
                XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
                fileOutputStream = new FileOutputStream(new File(filename));
                sortie.output(document, fileOutputStream);
            } catch (IOException e) {
                LOGGER.error("Impossible to export view configuration {}", e.getMessage());
                LOGGER.debug("Stack trace", e);
            } finally {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    LOGGER.error("Impossible to close view configuration file {} because {}", filename, e.getMessage());
                    LOGGER.debug("Stack trace", e);
                }
            }
        }
    }

    // Load configuration file
    @Override
    public void importViewConfiguration(final String filename) {
        Runnable viewConfigRunnable = new Runnable() {
            @Override
            public void run() {
                final Window mainWindow = WindowSwingUtils.getWindowForComponent(tangoDeviceExplorer);
                final Cursor cursor = (mainWindow == null ? null : mainWindow.getCursor());
                EDTManager.INSTANCE.runInDrawingThread(() -> {
                    if (mainWindow != null) {
                        mainWindow.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    }
                });

                final DataItemViewer dataItemViewer = getDataItemViewer();
                Document document = null;
                SAXBuilder sxb = new SAXBuilder();
                try {
                    document = sxb.build(new File(filename));
                    isDevicePanel(true);

                    if (document != null) {
                        // list of sources to load
                        List<LoadParameter> paramList = new ArrayList<>();

                        Map<Element, IKey> sourcesTable = new HashMap<>();
                        Map<Element, IDataSourceBrowser> browsersTable = new HashMap<>();

                        Element racine = document.getRootElement();

                        final DOMOutputter domOutputter = new DOMOutputter();

                        // load chart properties
                        Element elemChartProp = racine.getChild("chartParams");

                        if (elemChartProp != null) {
                            org.w3c.dom.Element chartPropNode = domOutputter.output(elemChartProp);
                            ChartProperties chartProperties = ChartPropertiesXmlManager
                                    .loadChartProperties(chartPropNode);
                            dataItemViewer.setChartProperties(chartProperties);
                        }

                        // Load docking view
                        Element child = racine.getChild("docking");
                        Element layoutElem = child.getChild("dataitemlayout");
                        String attributeValue = layoutElem.getAttributeValue("value");

                        // For DataItemViewer
                        byte[] decodeBuffer = stringToByteArray(attributeValue);
                        getDockingUtil().setLayout(decodeBuffer, dataItemViewer.getDockingArea(),
                                DockingUtil.ITEM_VIEW_ID);

                        // Load docking view

                        Element elemItems2 = racine.getChild("items");
                        if (elemItems2 != null) {// should not be null
                            List<Element> elemSourceList = elemItems2.getChildren("item");
                            for (Element elemSource : elemSourceList) {

                                Element elemSourceKey = elemSource.getChild("iKey");
                                try {
                                    org.w3c.dom.Element sourceKeyNode = domOutputter.output(elemSourceKey);
                                    IKey sourceKey = AbstractKey.recoverKeyFromXMLNode(sourceKeyNode);

                                    IDataSourceBrowser browser = DataSourceManager.getProducerFromKey(sourceKey);
                                    if (browser == null) {
                                        throw new LoadException(
                                                filename + " (source node #" + (1 + elemSourceList.indexOf(elemSource))
                                                        + ")",
                                                DataBrowser.MESSAGES.getString("Error.LoadSource.UnavailablePlugin")
                                                        + " " + sourceKey.getInformationKey());
                                    }

                                    sourcesTable.put(elemSource, sourceKey);
                                    browsersTable.put(elemSource, browser);

                                    // add source to load queue
                                    paramList.add(new KeyParameter(browser, sourceKey));
                                } catch (KeyCompatibilityException e) {
                                    LOGGER.error("Impossible to import view configuration {} because {}", filename,
                                            e.getMessage());
                                    LOGGER.debug("Stack trace", e);

                                } catch (LoadException e) {
                                    LOGGER.error("Impossible to load view configuration {} because {}", filename,
                                            e.getMessage());
                                    LOGGER.debug("Stack trace", e);
                                }
                            }
                        }

                        try {
                            // use this to wait for all sources to be loaded
                            // before trying to load items
                            CountDownLatch sourcesLatch = new CountDownLatch(paramList.size());

                            // load all valid sources
                            LOGGER.trace("Load sources {}", Arrays.toString(paramList.toArray()));

                            loadSources(paramList, sourcesLatch);
                            sourcesLatch.await();
                        } catch (InterruptedException e) {
                            LOGGER.error("InterruptedException {}", e.getMessage());
                            LOGGER.debug("Stack trace", e);
                        }

                        // go for loading items of successfully opened sources

                        for (Map.Entry<Element, IKey> sourceEntry : sourcesTable.entrySet()) {
                            Element elemSource = sourceEntry.getKey();
                            final IKey sourceKey = sourceEntry.getValue();
                            final IDataSourceBrowser browser = browsersTable.get(elemSource);

                            // only consider items from successfully opened
                            // sources
                            if (browser.isSourceOpened(sourceKey)) {
                                Element elemItems = racine.getChild("items");
                                if (elemItems != null) {// should not be null
                                    List<Element> elemItemList = elemItems.getChildren("item");
                                    for (final Element elemItem : elemItemList) {
                                        Element elemItemKey = elemItem.getChild("iKey");

                                        try {
                                            // recover the xml key
                                            org.w3c.dom.Element itemKeyNode = domOutputter.output(elemItemKey);
                                            final IKey itemKey = AbstractKey.recoverKeyFromXMLNode(itemKeyNode);

                                            final OpenItemStructure openStructure = new OpenItemStructure();
                                            openStructure.browser = browser;
                                            openStructure.sourceKey = sourceKey;
                                            openStructure.itemKey = itemKey;

                                            EDTManager.INSTANCE.runInDrawingThread(() -> {
                                                // open the item in DevicePanel
                                                openItem(openStructure.browser, openStructure.sourceKey,
                                                        openStructure.itemKey);
                                            });

                                        } catch (Exception e) {
                                            LOGGER.error("Impossible to import view configuration {} because {}",
                                                    filename, e.getMessage());
                                            LOGGER.debug("Stack trace", e);
                                        }

                                    }
                                }
                            }
                        }

                    }
                } catch (JDOMException e1) {
                    LOGGER.error("Impossible to import view configuration {} because {}", filename, e1.getMessage());
                    LOGGER.debug("Stack trace", e1);
                } catch (IOException e1) {
                    LOGGER.error("Impossible to import view configuration {} because {}", filename, e1.getMessage());
                    LOGGER.debug("Stack trace", e1);
                } finally {
                    EDTManager.INSTANCE.runInDrawingThread(() -> {
                        // cursor is null only if frame is null
                        if (cursor != null) {
                            mainWindow.setCursor(cursor);
                        }
                    });
                }
            }
        };

        new Thread(new FutureTask<>(viewConfigRunnable, null),
                "TangoDeviceExplorerController.importViewConfiguration(" + filename + ")").start();
    }

    public void createDefaultLayout() {
        getDockingUtil().setLayout(DockingUtil.DEFAULT_DATAITEM_LAYOUT, getDataItemViewer().getDockingArea(),
                DockingUtil.ITEM_VIEW_ID);
    }

    public void openItem(final List<String> selectedDevices) {
        openItem(selectedDevices, null, null);
    }

    public void closeItem(final String attribute) {
        if (openedAttribute != null && attribute != null) {
            Item item = openedAttribute.get(attribute.toLowerCase());
            if (item != null) {
                DataBrowserItemEvent event = new DataBrowserItemEvent(item.getKey(), item);
                getDataItemViewer().onItemClosed(event);
                openedAttribute.remove(attribute.toLowerCase());
            }
        }
    }

    public void closeAllOpenItem() {
        if (openedAttribute != null) {
            Set<Entry<String, Item>> entrySet = openedAttribute.entrySet();
            Item item = null;
            for (Entry<String, Item> entry : entrySet) {
                item = entry.getValue();
                DataBrowserItemEvent event = new DataBrowserItemEvent(item.getKey(), item);
                getDataItemViewer().onItemClosed(event);
            }
            openedAttribute.clear();
        }
    }

    protected boolean isNotCancelled(ICancelable cancelable) {
        return (cancelable == null) || (!cancelable.isCanceled());
    }

    protected boolean isCancelled(ICancelable cancelable) {
        return (cancelable != null) && cancelable.isCanceled();
    }

    public static IKey getSourceKey(IKey key, IKey knownSourceKey) {
        IKey sourceKey;
        String xDevice = TangoKeyTool.getDeviceName(key);
        String device = TangoKeyTool.getDeviceName(knownSourceKey);
        if (xDevice == null) {
            if (device == null) {
                sourceKey = knownSourceKey;
            } else {
                sourceKey = null;
            }
        } else if ((device != null) && device.equalsIgnoreCase(xDevice)) {
            sourceKey = knownSourceKey;
        } else {
            sourceKey = new TangoKey();
            TangoKeyTool.registerDeviceName(knownSourceKey, xDevice);
        }
        return sourceKey;
    }

    public void openItem(final IDataSourceBrowser browser, final IKey sourceKey, final IKey itemKey,
            DataType forcedType, AxisType forcedAxis, ICancelable cancelable, boolean cursorManagement) {
        // Display the waiting cursor
        if (isNotCancelled(cancelable) && (sourceKey != null) && (itemKey != null)) {
            IKey xScale = browser.getXScale(itemKey);
            IKey sliderScale = browser.getSliderScale(itemKey);

            DataFormat format = browser.getFormat(itemKey);
            Item item = new Item(itemKey, sourceKey, browser, forcedType, format);

            // if itemOpened is true you can't open the attribute because the attribute is already added
            String attributeName = TangoKeyTool.getAttributeName(itemKey);
            if (isNotCancelled(cancelable) && !isItemOpened(attributeName)) {
                openedAttribute.put(attributeName.toLowerCase(), item);
                final Frame frameForComponent = CometeUtils.getFrameForComponent(tangoDeviceExplorer);
                final Cursor cursor = (frameForComponent == null ? null : frameForComponent.getCursor());

                if (cursorManagement) {
                    EDTManager.INSTANCE.runInDrawingThread(() -> {
                        if (frameForComponent != null) {
                            frameForComponent.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                        }
                    });
                }

                try {
                    if (isNotCancelled(cancelable)) {
                        // Check if the item is already opened
                        if (!isItemOpened(sourceKey, itemKey)) {
                            // If the item is not open
                            List<IKey> itemKeys = openedItemsFromSource.get(sourceKey);
                            if (itemKeys != null) {
                                itemKeys.add(itemKey);
                            }

                            // multi-sources management
                            List<IKey> sources = openedSourcesForItem.get(itemKey);
                            if (sources == null) {
                                sources = new ArrayList<>();
                                openedSourcesForItem.put(itemKey, sources);
                            }
                            sources.add(sourceKey);

                            IKey xSourceKey = getSourceKey(xScale, sourceKey);

                            item.setXKey(xScale, xSourceKey);
                            if (forcedAxis != null) {
                                DataType currentType = browser.getType(itemKey);
                                if ((forcedAxis == AxisType.IMAGE) && (currentType != DataType.IMAGE)) {
                                    // In this case force a spectrum to image before see JIRA EXPDATA-215
                                    browser.setType(itemKey, DataType.IMAGE);
                                }
                                item.setAxis(forcedAxis);
                            }
                            items.put(itemKey, item);
                            fireItemOpened(item);
                        } else {
                            // The item is already open change only the axis
                            setItemAxis(itemKey, forcedAxis);
                        }
                    }
                    if (isNotCancelled(cancelable)) {
                        // ensure item has been added
                        // Set item selected in viewer
                        if (items.get(itemKey) != null) {
                            selectItems(itemKey);
                        }
                        tangoDeviceExplorer.getDataItemViewer().setValueConvertor(sliderScale);
                    }
                } finally {
                    if (cursorManagement) {
                        EDTManager.INSTANCE.runInDrawingThread(() -> {
                            // cursor is null only if frame is null
                            if (cursor != null) {
                                frameForComponent.setCursor(cursor);
                            }
                        });
                    }
                }
            }
        }
    }

    @Override
    public void openItem(final IDataSourceBrowser browser, final IKey sourceKey, final IKey itemKey,
            DataType forcedType, AxisType forcedAxis) {
        openItem(browser, sourceKey, itemKey, forcedType, forcedAxis, null, true);
    }

    public void openItem(final String attribute, DataType dataType, AxisType axisType) {
        DataType itemType = dataType;
        List<IKey> keysForAttributes = getKeysForAttributes(attribute);
        for (IKey key : keysForAttributes) {
            if (dataType == null) {
                itemType = dataSourceBrowser.getKeyType(key);
            }
            openItem(dataSourceBrowser, knownSourceKey, key, itemType, axisType);
        }
    }

    public void openItem(final List<String> selectedDevices, DataType dataType, AxisType axisType) {
        if (selectedDevices != null) {
            ProgressDialog progressDialog = getProgressDialog();
            progressDialog.setTitle("Opening items...");
            progressDialog.setMaxProgress(selectedDevices.size());
            progressDialog.setProgress(0, progressDialog.getTitle());
            progressDialog.setCloseOnMaxProgress(true);
            final Window mainWindow = WindowSwingUtils.getWindowForComponent(tangoDeviceExplorer);
            final Cursor cursor = (mainWindow == null ? null : mainWindow.getCursor());
//            MultiItemsOpener opener = new MultiItemsOpener(selectedDevices, dataType, axisType, cursor);
            ItemsOpener opener = new ItemsOpener(selectedDevices, dataType, axisType, cursor);
            progressDialog.setCancelable((ICancelable) opener);
            progressDialog.pack();
            progressDialog.setSize(Math.max(progressDialog.getWidth(), 300), progressDialog.getHeight());
            progressDialog.setLocationRelativeTo(mainWindow);
            progressDialog.setVisible(true);
//            opener.runNextWorker();
            opener.execute();
        }
    }

    public boolean isMatchingType(final IKey key, DataType dataType) {
        boolean match = false;
        DataType type = dataSourceBrowser.getKeyType(key);
        DataFormat format = dataSourceBrowser.getFormat(key);
        if ((type == dataType) || ((type != DataType.SCALAR) && (dataType != DataType.SCALAR))) {
            switch (type) {
                case SCALAR:
                    match = true;
                    break;
                case SPECTRUM:
                    switch (format) {
                        case BOOLEAN:
                        case NUMERICAL:
                            match = true;
                            break;
                        case TEXT:
                            if (dataType != DataType.IMAGE) {
                                match = true;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case IMAGE:
                    if (format != DataFormat.TEXT) {
                        match = true;
                    }
                    break;
                default:
                    break;
            }
        }
        return match;
    }

    public List<IKey> getKeysForAttributes(final String name) {
        List<IKey> keys = new ArrayList<>();
        IKey generateReadableKey = dataSourceBrowser.generateReadableKey(getDeviceName(), name);
        if (generateReadableKey != null) {
            keys.add(generateReadableKey);
            IKey writeKey = dataSourceBrowser.generateSettableKey(generateReadableKey);
            if (writeKey != null) {
                keys.add(writeKey);
            }
        }
        return keys;
    }

    public boolean isItemOpened(final String name) {
        boolean isOpened = false;
        if (name != null) {
            isOpened = openedAttribute.containsKey(name.toLowerCase());
        }
        if (!isOpened) {
            if (getDeviceStatePanel() != null) {
                List<String> openedCommand = getDeviceStatePanel().getOpenedCommand();
                if (openedCommand != null) {
                    isOpened = openedCommand.contains(name);
                }
            }
        }
        return isOpened;
    }

    public Map<String, Item> getOpenedAttribute() {
        return openedAttribute;
    }

    public List<Item> getOpenedSpectrumImageAttribute() {
        if (openedSpectrumImageAttribute == null) {
            openedSpectrumImageAttribute = new ArrayList<>();
            openedSpectrum = new ArrayList<>();
            openedImage = new ArrayList<>();
        }
        openedSpectrumImageAttribute.clear();
        openedSpectrum.clear();
        openedImage.clear();
        if (openedAttribute != null && !openedAttribute.isEmpty()) {
            Collection<Item> values = openedAttribute.values();
            for (Item itemOpened : values) {
                DataType type = itemOpened.getType();
                if ((DataType.IMAGE.equals(type) || DataType.SPECTRUM.equals(type))) {
                    openedSpectrumImageAttribute.add(itemOpened);
                    if (DataType.SPECTRUM.equals(type) && !openedSpectrum.contains(itemOpened)) {
                        openedSpectrum.add(itemOpened);
                    } else if (!openedImage.contains(itemOpened)) {
                        openedImage.add(itemOpened);
                    }
                }
            }
        }
        return openedSpectrumImageAttribute;
    }

    public List<Item> getOpenedSpectrum() {
        return openedSpectrum;
    }

    public List<Item> getOpenedImage() {
        return openedImage;
    }

    public List<String> getAllScalarList() {
        List<String> scalarList = getTangoSourceDevice().getAttributeSource().getScalarList();
        return scalarList;
    }

    public List<String> getAllImageList() {
        List<String> imageList = getTangoSourceDevice().getAttributeSource().getImageList();
        return imageList;
    }

    public List<String> getAllSpectrumList() {
        List<String> spectrumList = getTangoSourceDevice().getAttributeSource().getSpectrumList();
        return spectrumList;
    }

    public void executeCommand(String command) {
        boolean argin = sourceDevice.getCommandSource().needArgin(command);
        Object result = null;
        if (argin) {
            openDialogCommands(command);
        } else {
            result = sourceDevice.getCommandSource().executeVoidCommand(command);
        }

        if (result != null && !(result instanceof Exception)) {
            openDialogCommandResult(command, String.valueOf(result));
        } else if (result instanceof Exception) {
            JXErrorPane.showDialog((Exception) result);
        }
    }

    private void openDialogCommandResult(String commandName, String result) {
        if (executionCommandDialog == null) {
            executionCommandDialog = new ExecutionCommandDialog(
                    WindowSwingUtils.getWindowForComponent(tangoDeviceExplorer), this);
            executionCommandDialog.setLocationRelativeTo(null);
            executionCommandDialog.setSize(600, 500);
        }
        executionCommandDialog.showResult(commandName, result);
    }

    private void openDialogCommands(String commandName) {
        if (executionCommandDialog == null) {
            executionCommandDialog = new ExecutionCommandDialog(
                    WindowSwingUtils.getWindowForComponent(tangoDeviceExplorer), this);
            executionCommandDialog.setLocationRelativeTo(null);
            executionCommandDialog.setSize(600, 500);
        }
        executionCommandDialog.show(commandName);
    }

    public void addCommand(final String commandName) {
        DeviceStatePanel deviceStatePanel = getDeviceStatePanel();
        if (deviceStatePanel != null) {
            deviceStatePanel.addCommand(commandName);
        }
    }

    public List<String> getOpenedCommand() {
        List<String> openedCommand = null;
        DeviceStatePanel deviceStatePanel = getDeviceStatePanel();
        if (deviceStatePanel != null) {
            openedCommand = deviceStatePanel.getOpenedCommand();
        }
        return openedCommand;
    }

    public void removeAllCommand() {
        DeviceStatePanel deviceStatePanel = getDeviceStatePanel();
        if (deviceStatePanel != null) {
            deviceStatePanel.removeAllCommand();
        }
    }

    public TangoCommand getCommandArginType(String commandName) {
        TangoCommand command = null;
        try {
            command = sourceDevice.getCommandSource().getTangoCommand(commandName);
        } catch (Exception e) {
            command = null;
        }
        return command;

    }

    public void traceMessage(String message) {
        LOGGER.info(message);
    }

    public void traceMessage(String message, Object... arguments) {
        LOGGER.info(message, arguments);
    }

    public void traceError(String errorMessage, Exception e) {
        LOGGER.error(errorMessage + ":{}", e.getMessage());
        LOGGER.debug("Stack trace", e);
    }

    public void traceError(String errorMessage, Exception e, Object... arguments) {
        LOGGER.error(errorMessage + ":{}", arguments, e.getMessage());
        LOGGER.debug("Stack trace", e);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class ItemsOpener extends SwingWorker<Void, String> implements ICancelable {

        private List<String> selectedDevices;
        private DataType dataType;
        private AxisType axisType;
        private volatile boolean canceled;
        private Cursor cursor;
//        private long startTime;

        public ItemsOpener(List<String> selectedDevices, DataType dataType, AxisType axisType, Cursor cursor) {
            super();
            this.selectedDevices = selectedDevices;
            this.dataType = dataType;
            this.axisType = axisType;
            this.cursor = cursor;
            canceled = false;
        }

        @Override
        public void setCanceled(boolean canceled) {
            this.canceled = canceled;
        }

        @Override
        public boolean isCanceled() {
            return canceled;
        }

        @Override
        protected Void doInBackground() throws Exception {
//            startTime = System.currentTimeMillis();
            List<IKey> keysForAttributes = null;
            DataType itemType = dataType;
            for (String device : selectedDevices) {
                if (isCanceled()) {
                    break;
                }
                keysForAttributes = getKeysForAttributes(device);
                for (IKey key : keysForAttributes) {
                    if (isCanceled()) {
                        break;
                    }
                    if (dataType == null) {
                        itemType = dataSourceBrowser.getKeyType(key);
                    }
                    if (isCanceled()) {
                        break;
                    } else {
                        openItem(dataSourceBrowser, knownSourceKey, key, itemType, axisType, this, false);
                    }
                } // end for (IKey key : keysForAttributes)
                publish(device);
            }
            return null;
        }

        @Override
        protected void process(List<String> chunks) {
            for (@SuppressWarnings("unused")
            String key : chunks) {
                getProgressDialog().setProgress(getProgressDialog().getProgress() + 1);
            }
        }

        @Override
        protected void done() {
            try {
                get();
                getProgressDialog().setProgress(getProgressDialog().getMaxProgress());
            } catch (InterruptedException e) {
                LOGGER.error("Error while opening items", e.getCause());
            } catch (ExecutionException e) {
                setCanceled(true);
            } finally {
                getProgressDialog().setVisible(false);
                if (cursor != null) {
                    Window parent = WindowSwingUtils.getWindowForComponent(tangoDeviceExplorer);
                    if (parent != null) {
                        parent.setCursor(cursor);
                    }
                }
//                System.err.println(DateUtil.elapsedTimeToStringBuilder(new StringBuilder("****** It took "),
//                        System.currentTimeMillis() - startTime).append(" to open all items"));
            }
        }

    }

}
