package fr.soleil.comete.bean.tangodeviceexplorer.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.WindowConstants;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import org.cdma.gui.databrowser.DataBrowser;
import org.cdma.gui.databrowser.SettingsManager;
import org.cdma.gui.databrowser.interfaces.AxisType;
import org.cdma.gui.databrowser.interfaces.DataType;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.model.DataItemViewer;
import org.cdma.gui.databrowser.util.ApplicationUtil;
import org.cdma.gui.databrowser.util.MultiExtFileFilter;
import org.cdma.gui.databrowser.util.SingleFileChooser;
import org.jdesktop.swingx.JXErrorPane;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.tangodeviceexplorer.controller.TangoDeviceExplorerController;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.lib.project.file.BatchExecutor;
import fr.soleil.lib.project.swing.Splash;

public class TangoDeviceExplorer extends AbstractDeviceExplorerPanel {

    private static final long serialVersionUID = 5346025405415608105L;

    private static final Logger LOGGER = LoggerFactory.getLogger(TangoDeviceExplorer.class);

    private static final String CONFIG_FILE_PROPERTY = "DEVICEPANEL_CONFIGFILE";

    private static final String ARGS_SPECIFICCONFIG_FILE = "-specificconfig";
    private static final String ARGS_CONFIG_FILE = "-configfile";
    private static final String ARGS_OPEN_ALL = "-openall";
    private static final String ARGS_OPEN_ALL_SCALAR = "-openallscalar";
    private static final String ARGS_OPEN_ALL_IMAGE = "-openallimage";
    private static final String ARGS_OPEN_ALL_SPECTRUM = "-openallspectrum";
    private static final String ARGS_HIDE_STATUS = "-hidestatus";

    private static final String ARGS_HELP = "-help";

    private static final String CLASS_CONFIGFILE = "[CLASS]";
    private static final String DEVICE_CONFIGFILE = "[DEVICE]";

    private static final String FRAME_TITLE = "DevicePanel";

    private JSplitPane splitPane = new JSplitPane();
    private DataItemViewer dataItemViewer;
    private DeviceTreePanel deviceTreePanel;

    private DeviceStatePanel deviceStatePanel;
    private TangoDeviceExplorerController controller = null;

    private static JMenuBar menuBar;

    private JMenu menuFile;
    private JMenu menuHelp;
    private JMenu menuStandardCommands;
    private JMenu menuPreference;

    private JMenuItem menuQuitItem;
    private JMenuItem menuSaveConfigurationFile;
    private JMenuItem menuLoadConfigurationFile;
    private JMenuItem menuInitItem;
    private JMenuItem menuStatetItem;
    private JMenuItem menuStatustItem;

    private JMenuItem menuResetLayout;
    private JMenuItem menuStopRefreshingItem;
    private JMenuItem menuRefreshOnceItem;
    private JMenuItem menuSetRefreshingPeriodItem;
    private JMenuItem menuSetDeviceTimeOutItem;
    private JMenuItem menuShowDeviceStatePanel;
    private JMenuItem menuHideDeviceStatePanel;
    private JCheckBoxMenuItem menuExpertView;
    private JCheckBoxMenuItem menuShowAllScalarLabel;

    private JMenuItem menuHelpItem;

    private JFileChooser viewConfigFilechooser;

    private JFrame frame = null;
    private HelpDialog helpDialog = null;
    private String deviceName;

    private SettingsManager settings;

    private static boolean isOpenAllScalar = false;
    private static boolean isOpenAllSpectrum = false;
    private static boolean isOpenAllImage = false;
    private static boolean isOpenAllSource = false;
    private static boolean hideStatus = false;
    private static boolean openConfigFile = false;

    private static Map<String, String> classMap = new HashMap<String, String>();
    private static Map<String, String> deviceMap = new HashMap<String, String>();

    public TangoDeviceExplorer(String deviceName) {
        this.deviceName = deviceName;
        initComponent();
    }

    private void initComponent() {

        TangoDeviceExplorerController controller = new TangoDeviceExplorerController(this);
        this.controller = controller;
        controller.setDeviceName(deviceName);

        setLayout(new BorderLayout());
        add(splitPane, BorderLayout.CENTER);

        dataItemViewer = new DataItemViewer(controller, true);
        controller.addDataBrowserItemListener(dataItemViewer);

        deviceTreePanel = new DeviceTreePanel(controller);
        deviceStatePanel = new DeviceStatePanel(controller);

        TreeCommandTransferHandler treeCommandTransferHandler = new TreeCommandTransferHandler(controller,
                deviceTreePanel, deviceStatePanel);
        deviceStatePanel.setCommandTransferHandler(treeCommandTransferHandler);

        dataItemViewer.transferHandlerForScalarViewer(new TreeItemTransferHandler(controller, deviceTreePanel));
        dataItemViewer.transferHandlerForMatrixViewer(
                new TreeItemTransferHandler(controller, DataType.IMAGE, AxisType.IMAGE, deviceTreePanel));
        dataItemViewer.transferHandlerForSpectrumViewer(
                new TreeItemTransferHandler(controller, DataType.SPECTRUM, AxisType.Y1, deviceTreePanel));

        createMenuBar();

        splitPane.setRightComponent(dataItemViewer.getDockingArea());
        splitPane.setLeftComponent(deviceTreePanel);

        add(deviceStatePanel, BorderLayout.NORTH);

        String configFilename = System.getProperty(CONFIG_FILE_PROPERTY);
        settings = new SettingsManager(configFilename);

        if (isOpenAllSource) {
            deviceTreePanel.openAllSources();
        } else if (isOpenAllScalar) {
            deviceTreePanel.openAllScalar();
        } else if (isOpenAllSpectrum) {
            deviceTreePanel.openAllSpectrum();
        } else if (isOpenAllImage) {
            deviceTreePanel.openAllImage();
        }

        if (hideStatus) {
            deviceStatePanel.setStatusVisible(false);
        }
    }

    public DeviceStatePanel getDeviceStatePanel() {
        return deviceStatePanel;
    }

    public DataItemViewer getDataItemViewer() {
        return dataItemViewer;
    }

    private void createMenuBar() {
        menuBar = new JMenuBar();
        menuFile = new JMenu("File");
        menuStandardCommands = new JMenu("Standard commands");
        menuPreference = new JMenu("Preference");
        menuHelp = new JMenu("Help");

        menuQuitItem = new JMenuItem("Quit");
        menuSaveConfigurationFile = new JMenuItem("Save conguration file");
        menuLoadConfigurationFile = new JMenuItem("Load configuration file");
        menuInitItem = new JMenuItem("Init");
        menuStatetItem = new JMenuItem("State");
        menuStatustItem = new JMenuItem("Status");

        menuResetLayout = new JMenuItem("Reset layout");
        menuStopRefreshingItem = new JMenuItem("Stop refreshing");
        menuRefreshOnceItem = new JMenuItem("Refresh once");
        menuSetRefreshingPeriodItem = new JMenuItem("Set refreshing period...");
        menuSetDeviceTimeOutItem = new JMenuItem("Set device time out...");
        menuShowDeviceStatePanel = new JMenuItem("Show Status");
        menuHideDeviceStatePanel = new JMenuItem("Hide Status");
        menuExpertView = new JCheckBoxMenuItem("Expert");
        menuExpertView.setSelected(false);
        menuShowAllScalarLabel = new JCheckBoxMenuItem("Show full scalar label");
        menuShowAllScalarLabel.setSelected(false);

        menuHideDeviceStatePanel.setEnabled(!hideStatus);
        menuShowDeviceStatePanel.setEnabled(hideStatus);

        menuHelpItem = new JMenuItem("About...");

        menuSaveConfigurationFile.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                initViewConfigFilechooser();
                int result = viewConfigFilechooser.showSaveDialog(TangoDeviceExplorer.this);
                if (result == JFileChooser.APPROVE_OPTION) {
                    saveViewConfigFile(viewConfigFilechooser.getSelectedFile().getAbsolutePath());
                }
            }
        });

        menuLoadConfigurationFile.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                initViewConfigFilechooser();
                int result = viewConfigFilechooser.showOpenDialog(TangoDeviceExplorer.this);
                if (result == JFileChooser.APPROVE_OPTION) {
                    loadViewConfigFile(viewConfigFilechooser.getSelectedFile().getPath());
                }
            }
        });

        menuResetLayout.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                controller.createDefaultLayout();
            }
        });

        menuQuitItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        menuHelpItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (helpDialog == null) {
                    helpDialog = new HelpDialog(frame);
                }
                helpDialog.setVisible(true);
                helpDialog.toFront();
            }
        });

        menuSetRefreshingPeriodItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                showRefreshingPeriodDialog();
            }
        });

        menuStopRefreshingItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                controller.stopRefreshingPeriod();
            }
        });

        menuSetDeviceTimeOutItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                showDialogDeviceTimeOut();
            }
        });

        menuInitItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                int option = JOptionPane.showConfirmDialog(null, "Are you sure ?", "Execute init command",
                        JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                if (option == JOptionPane.YES_OPTION) {
                    DeviceProxy deviceProxy = controller.getDeviceProxy();
                    if (deviceProxy != null) {
                        try {
                            deviceProxy.command_inout(TangoDeviceExplorerUtilities.COMMAND_INIT);
                        } catch (DevFailed e1) {
                            LOGGER.debug("Exception init command failed {}", e);
                            e1.printStackTrace();
                        }
                    }
                } else {
                    LOGGER.debug("Cancel INIT command");
                }
            }
        });

        menuStatetItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                DeviceProxy deviceProxy = controller.getDeviceProxy();
                if (deviceProxy != null) {
                    try {
                        deviceProxy.command_inout(TangoDeviceExplorerUtilities.COMMAND_STATE);
                    } catch (DevFailed e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        menuStatustItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                DeviceProxy deviceProxy = controller.getDeviceProxy();
                if (deviceProxy != null) {
                    try {
                        deviceProxy.command_inout(TangoDeviceExplorerUtilities.COMMAND_Status);
                    } catch (DevFailed e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        menuShowDeviceStatePanel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                deviceStatePanel.setStatusVisible(true);
                menuHideDeviceStatePanel.setEnabled(true);
                menuShowDeviceStatePanel.setEnabled(false);
            }
        });

        menuHideDeviceStatePanel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                deviceStatePanel.setStatusVisible(false);
                menuHideDeviceStatePanel.setEnabled(false);
                menuShowDeviceStatePanel.setEnabled(true);
            }
        });

        menuExpertView.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                boolean selected = menuExpertView.isSelected();
                // System.out.println("menuExpertView=" +selected );
                controller.setExpert(selected);
                deviceTreePanel.setModel(model);
            }
        });

        menuShowAllScalarLabel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                boolean selected = menuShowAllScalarLabel.isSelected();
                controller.setShowFullScalarLabel(selected);
            }
        });

        menuPreference.addMenuListener(new MenuListener() {

            @Override
            public void menuSelected(MenuEvent e) {
                Map<String, Item> openedAttribute = controller.getOpenedAttribute();
                if (openedAttribute.size() > 0) {
                    menuSetRefreshingPeriodItem.setEnabled(true);
                    menuStopRefreshingItem.setEnabled(true);
                } else {
                    menuSetRefreshingPeriodItem.setEnabled(false);
                    menuStopRefreshingItem.setEnabled(false);
                }
            }

            @Override
            public void menuDeselected(MenuEvent e) {
                // do nothing

            }

            @Override
            public void menuCanceled(MenuEvent e) {
                // do nothing
            }
        });

        menuFile.add(menuSaveConfigurationFile);
        menuFile.add(menuLoadConfigurationFile);
        menuFile.addSeparator();
        menuFile.add(menuQuitItem);

        menuStandardCommands.add(menuInitItem);
        menuStandardCommands.add(menuStatetItem);
        menuStandardCommands.add(menuStatustItem);

        menuPreference.add(menuStopRefreshingItem);
        menuPreference.add(menuRefreshOnceItem);
        menuPreference.add(menuSetRefreshingPeriodItem);
        menuPreference.add(menuSetDeviceTimeOutItem);
        menuPreference.addSeparator();
        menuPreference.add(menuShowDeviceStatePanel);
        menuPreference.add(menuHideDeviceStatePanel);
        menuPreference.addSeparator();
        menuPreference.add(menuShowAllScalarLabel);
        menuPreference.addSeparator();
        menuPreference.add(menuExpertView);

        menuHelp.add(menuResetLayout);
        menuHelp.addSeparator();
        menuHelp.add(menuHelpItem);

        menuBar.add(menuFile);
        menuBar.add(menuStandardCommands);
        menuBar.add(menuPreference);
        menuBar.add(menuHelp);

    }

    public void saveViewConfigFile(final String viewFileName) {
        controller.exportViewConfiguration(viewFileName);
    }

    private void loadViewConfigFile(String viewFileName) {
        controller.importViewConfiguration(viewFileName);
    }

    private void initViewConfigFilechooser() {
        if (viewConfigFilechooser == null) {
            viewConfigFilechooser = new SingleFileChooser();

            MultiExtFileFilter viewConfigFileFilter = new MultiExtFileFilter("Fichier de configuration DevicePanel",
                    "xml");
            viewConfigFilechooser.setFileFilter(viewConfigFileFilter);

            String viewConfigLocation = settings.getViewConfigLocation();
            if (viewConfigLocation != null) {
                viewConfigFilechooser.setCurrentDirectory(new File(viewConfigLocation));
            }
        }
    }

    @Override
    public void setModel(String arg0) {
        super.setModel(arg0);
        deviceStatePanel.setModel(arg0);
        deviceTreePanel.setModel(arg0);
    }

    @Override
    public void start() {
        super.start();
        deviceStatePanel.start();
        // deviceTreePanel.start();
    }

    @Override
    protected void clearGUI() {
        // TODO Auto-generated method stub
    }

    @Override
    protected void refreshGUI() {
        // TODO Auto-generated method stub
    }

    private void showRefreshingPeriodDialog() {
        Object refreshPeriodAnwser = JOptionPane.showInputDialog(this, "Enter refresh period (ms)", refreshingTime);
        if (refreshPeriodAnwser != null) {
            try {
                int parseInt = Integer.parseInt(refreshPeriodAnwser.toString());
                refreshingTime = parseInt;
                controller.setRefreshingPeriod(parseInt);

                // TODO replace by when JAVAAPI-402
                // setRefreshingTime(parseInt);
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, e.getMessage(), "Refreshing period error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }

    }

    private void showDialogDeviceTimeOut() {
        int timeOutInMilliseconds = TangoDeviceHelper.getTimeOutInMilliseconds(deviceName);
        Object refreshPeriodAnwser = JOptionPane.showInputDialog(this, "Enter timeout for device (ms)",
                timeOutInMilliseconds);
        if (refreshPeriodAnwser != null) {
            try {
                int parseInt = Integer.parseInt(refreshPeriodAnwser.toString());
                controller.setDeviceTimeOut(parseInt);
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, e.getMessage(), "Timeout period error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void createFrame(TangoDeviceExplorer explorer) {

        frame = new JFrame();
        frame.setContentPane(explorer);

        explorer.setModel(deviceName);
        explorer.start();

        frame.setJMenuBar(menuBar);
        String version = ApplicationUtil.getFileJarVersion(TangoDeviceExplorer.class);
        String frameTitle = FRAME_TITLE + " - " + controller.getDeviceName();
        if ((version != null) && !version.isEmpty()) {
            frameTitle = frameTitle + " - " + version;
        }
        frame.setTitle(frameTitle);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.toFront();
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setSize(800, 800);
        ImageIcon icon = TangoDeviceExplorerUtilities.ICONS.getIcon("Application.TangoDeviceExplorer");
        frame.setIconImage(icon.getImage());

        if (isOpenAllSource) {
            deviceTreePanel.openAllSources();
        } else if (isOpenAllScalar) {
            deviceTreePanel.openAllScalar();
        } else if (isOpenAllSpectrum) {
            deviceTreePanel.openAllSpectrum();
        } else if (isOpenAllImage) {
            deviceTreePanel.openAllImage();
        }
    }

    private static boolean readConfigFile(String configFile) {

        boolean ok = false;
        boolean isClass = false;
        boolean isDevice = false;

        if (configFile != null && !configFile.isEmpty()) {
            File file = new File(configFile);

            if (file.exists()) {
                BufferedReader reader = null;
                try {
                    String line = null;
                    reader = new BufferedReader(new FileReader(configFile));
                    String nameClass = null;
                    String execName = null;
                    String devName = null;
                    while ((line = reader.readLine()) != null) {
                        line = line.trim();

                        if (line.startsWith("!") || line.equals("\n")) {
                            // Ignore comment start with ! and backspace
                        } else if (line.contains(CLASS_CONFIGFILE)) {
                            isClass = true;
                            isDevice = false;
                        } else if (line.contains(DEVICE_CONFIGFILE)) {
                            isDevice = true;
                            isClass = false;
                        } else if (isClass) {
                            String[] split = line.split("=");
                            if (split.length > 1) {
                                nameClass = split[0].trim();
                                execName = split[1].trim();
                                classMap.put(nameClass, execName);
                                LOGGER.info("executable {} for class {}", execName, nameClass);
                            }

                        } else if (isDevice) {
                            String[] split = line.split("=");
                            if (split.length > 1) {
                                devName = split[0].trim();
                                execName = split[1].trim().toLowerCase();
                                deviceMap.put(devName, execName);
                                LOGGER.info("executable {} for device {}", execName, devName);
                            }
                        }
                    }
                    ok = true;
                } catch (IOException e) {
                    LOGGER.trace("Error when reading {}", configFile, e);
                    LOGGER.error("Error when reading {} because of {}", configFile, e.getMessage());
                    ok = false;
                } catch (Exception e) {
                    LOGGER.trace("Error when reading {}", configFile, e);
                    LOGGER.error("Error when reading {} because of {}", configFile, e.getMessage());
                    ok = false;
                }
            }
        }

        return ok;
    }

    private static String batchNameForDevice(String deviceName) {
        String execName = null;
        if (deviceName != null) {
            try {
                execName = deviceMap.get(deviceName.toLowerCase().trim());
                if (execName == null) {
                    String className = TangoDeviceHelper.getDeviceClass(deviceName.toLowerCase().trim());
                    LOGGER.info("Class {} for device {}", className, deviceName);
                    execName = classMap.get(className);
                }
            } catch (Exception e) {
                // FOR NPE
                e.printStackTrace();
                LOGGER.trace("Error when getting batch for device {}", deviceName, e);
                LOGGER.error("Error when getting batch for device {}", deviceName, e.getMessage());
                execName = null;
            }
        }
        if (execName == null) {
            LOGGER.info("No executable found for {} ", deviceName);
        }
        return execName;
    }

    public static void main(String[] args) {

        // Display help
        if (args != null && args.length > 0 && args[0].equals(ARGS_HELP)) {
            System.out.println("[device_name]\t\t\t\tLaunch devicePanel on the specified device");
            System.out.println(ARGS_CONFIG_FILE + " [config_file_path]\t"
                    + "\t\t\t\tLauch device panel with configuration (docking and attributes).");
            System.out.println(ARGS_SPECIFICCONFIG_FILE + " [config_file_path]\t"
                    + "Launch a specific application according to the class of the device see $LIVE_ROOT/conf/devicepanel/device_panel_config.txt");
            System.out.println(
                    ARGS_OPEN_ALL + "\t\t\t\tDisplay all the attribute, caution this function may take a while");
            System.out.println(ARGS_OPEN_ALL_SCALAR
                    + "\t\t\t\tDisplay all scalar attribute, caution this function may take a while");
            System.out.println(ARGS_OPEN_ALL_IMAGE
                    + "\t\t\t\tDisplay all image attribute, caution this function may take a while");
            System.out.println(ARGS_OPEN_ALL_SPECTRUM
                    + "\t\t\t\tDisplay all spectrum attribute, caution this function may take a while");
            System.out.println(ARGS_HIDE_STATUS + "\t\t\t\tHide the status of the device by default");
            System.out.println(ARGS_HELP + "\t\t\t\t\t" + "Display this help");
            System.exit(0);
        } else {

            if (args.length > 0) {
                LOGGER.info("Arguments ", Arrays.toString(args));
            } else {
                LOGGER.info("DevicePanel : Without argument");
            }

            Splash splash = DataBrowser.SPLASH;
            splash.setTitle("Tango Device Explorer application");
            splash.setCopyright("(c) Synchrotron Soleil 2016");
            splash.setMessage("Starting application...");
            splash.setAlwaysOnTop(false);
            splash.progress(5);
            splash.setMessage("Create Frame application ...");

            // Read device name
            splash.progress(10);
            splash.setMessage("Read the arguments ...");

            String tmpDeviceName = null;
            String configFile = null;
            boolean readConfigFile = false;
            int indexConfigFile = -1;
            int indexOpenAll = -1;
            int indexOpenAllScalar = -1;
            int indexOpenAllImage = -1;
            int indexOpenAllSpectrum = -1;
            int indexHideStatus = -1;

            if (args != null && args.length > 0) {
                String arg = null;
                for (int i = 0; i < args.length; i++) {
                    arg = args[i];
                    if (arg.equals(ARGS_SPECIFICCONFIG_FILE)) {
                        if (i + 1 < args.length) {
                            indexConfigFile = i + 1;
                            configFile = args[indexConfigFile];
                            LOGGER.info("Read configuration file {}", configFile);
                            splash.progress(20);
                            readConfigFile = TangoDeviceExplorer.readConfigFile(configFile);
                        }
                    } else if (arg.equals(ARGS_OPEN_ALL)) {
                        indexOpenAll = i;
                        splash.progress(25);
                        splash.setMessage("open all attributes");
                        isOpenAllSource = true;
                    } else if (arg.equals(ARGS_OPEN_ALL_SCALAR)) {
                        indexOpenAllScalar = i;
                        splash.progress(25);
                        splash.setMessage("open all scalars");
                        isOpenAllScalar = true;
                    } else if (arg.equals(ARGS_OPEN_ALL_SPECTRUM)) {
                        indexOpenAllSpectrum = i;
                        splash.progress(25);
                        splash.setMessage("open all spectrums");
                        isOpenAllSpectrum = true;
                    } else if (arg.equals(ARGS_OPEN_ALL_IMAGE)) {
                        indexOpenAllImage = i;
                        splash.progress(25);
                        splash.setMessage("open all images");
                        isOpenAllImage = true;
                    } else if (arg.equals(ARGS_HIDE_STATUS)) {
                        indexHideStatus = i;
                        hideStatus = true;
                    } else if (arg.equals(ARGS_CONFIG_FILE)) {
                        if (i + 1 < args.length) {
                            String deviceNameWithXMLFile = null;
                            indexConfigFile = i + 1;
                            configFile = args[indexConfigFile];
                            Document document = null;
                            SAXBuilder sxb = new SAXBuilder();
                            try {
                                System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
                                document = sxb.build(new File(configFile));
                                if (document != null) {
                                    Element racine = document.getRootElement();
                                    Element elemDevice = racine.getChild("device");
                                    Element elemDeviceName = elemDevice.getChild("devicename");
                                    deviceNameWithXMLFile = elemDeviceName.getAttributeValue("value");
                                }
                            } catch (JDOMException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            tmpDeviceName = deviceNameWithXMLFile;
                            openConfigFile = true;
                        }

                    } else if (i != indexConfigFile && i != indexOpenAll && i != indexHideStatus
                            && i != indexOpenAllScalar && i != indexOpenAllImage && i != indexOpenAllSpectrum) {
                        tmpDeviceName = arg;
                    }
                }
            }

            if (tmpDeviceName == null || tmpDeviceName.isEmpty()) {
                splash.progress(35);
                splash.setVisible(false);
                splash.setMessage("Need device name to continue...");
                String message = "Please enter the device name";
                String title = "Enter device name";
                int messageType = JOptionPane.QUESTION_MESSAGE;
                tmpDeviceName = JOptionPane.showInputDialog(splash, message, title, messageType);
                splash.setVisible(true);
            }

            if (tmpDeviceName != null && !tmpDeviceName.isEmpty()) {
                splash.progress(60);
                LOGGER.info("Device selected {}", tmpDeviceName);
                String batchNameForDevice = batchNameForDevice(tmpDeviceName);
                LOGGER.info("batchNameForDevice {}", batchNameForDevice);

                if (!readConfigFile || batchNameForDevice == null) {
                    splash.progress(70);
                    splash.setMessage("Launch Tango Device Panel");
                    TangoDeviceExplorer explorer = new TangoDeviceExplorer(tmpDeviceName);
                    explorer.createFrame(explorer);
                    if (openConfigFile) {
                        explorer.loadViewConfigFile(configFile);
                    }
                } else {
                    splash.progress(70);
                    splash.setMessage("Launch " + batchNameForDevice);

                    BatchExecutor batchExecutor = new BatchExecutor();
                    List<String> param = new ArrayList<String>();
                    param.add(tmpDeviceName.trim().toLowerCase());
                    batchExecutor.setBatch(batchNameForDevice);
                    batchExecutor.setBatchParameters(param);
                    try {
                        batchExecutor.execute();
                    } catch (Exception e) {
                        JXErrorPane.showFrame(e);
                        splash.progress(80);
                        splash.setMessage("Launch Tango Device Panel");
                        TangoDeviceExplorer explorer = new TangoDeviceExplorer(tmpDeviceName);
                        explorer.createFrame(explorer);
                    }
                }
                splash.progress(100);
                splash.setMessage("Panel launched");
                splash.setVisible(false);

            } else {
                System.out.println("No device specified => Exit application");
                LOGGER.info("No device specified => Exit application");
                System.exit(0);
            }
        }

    }
}
