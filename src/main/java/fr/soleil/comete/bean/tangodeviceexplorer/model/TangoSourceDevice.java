package fr.soleil.comete.bean.tangodeviceexplorer.model;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.comete.bean.tangodeviceexplorer.controller.TangoDeviceExplorerController;
import fr.soleil.comete.bean.tangodeviceexplorer.view.TangoDeviceExplorerUtilities;
import fr.soleil.comete.bean.tangotree.SourceDeviceException;
import fr.soleil.comete.bean.tangotree.datasource.ISourceDevice;

public class TangoSourceDevice extends AbstractTangoDeviceSource implements ISourceDevice {

    private TangoAttributeSourceDevice attributeSource = null;
    private TangoCommandSourceDevice commandSource = null;
    
    public TangoSourceDevice(TangoDeviceExplorerController controller) {
        super(controller);
        attributeSource = new TangoAttributeSourceDevice(controller);
        commandSource = new TangoCommandSourceDevice(controller);
    }

    @Override
    public void setExpertLevel(boolean expertLevel) {
        attributeSource.setExpertLevel(expertLevel);
        commandSource.setExpertLevel(expertLevel);
        super.setExpertLevel(expertLevel);
    }
    
    

    @Override
    public void createSourceList() {
        
        // First add command
        commandSource.createSourceList();
        
        // Then add attribute
        attributeSource.createSourceList();
        
    }

    @Override
    public List<String> getSourceList() throws SourceDeviceException {
        createSourceList();
        
        List<String> sourceNodeList = new ArrayList<String>();
        
        List<String> cmdSourceList = commandSource.getDataSourceList();
        sourceNodeList.addAll(cmdSourceList);
        
        List<String> attrSourceList = attributeSource.getDataSourceList();
        sourceNodeList.addAll(attrSourceList);

        return sourceNodeList;

    }
    
    public List<String> getAttributeList() {
        return attributeSource.getAttributeList();
    }
    
    @Override
    public List<String> getDataSourceList() {
        List<String> sourceNodeList = new ArrayList<String>();
        
        List<String> cmdSourceList = commandSource.getDataSourceList();
        sourceNodeList.addAll(cmdSourceList);
        
        List<String> attrSourceList = attributeSource.getDataSourceList();
        sourceNodeList.addAll(attrSourceList);

        return sourceNodeList;
    }

    @Override
    public String getName() {
        return getDeviceName();

    }

    @Override
    public String getInformation() {
        return getName();
    }

    @Override
    public String getPathSeparator() {
        return TangoDeviceExplorerUtilities.PATH_SEPARATOR;
    }

    @Override
    public boolean contains(String key) {
        boolean contains = containsAttribute(key);
        if (!contains) {
            contains = containsCommand(key);
        }
        return contains;
    }

    public boolean containsAttribute(final String key) {
        return attributeSource.contains(key);
    }

    public boolean containsCommand(final String key) {
        return commandSource.contains(key);
    }
    
    public TangoCommandSourceDevice getCommandSource(){
        return commandSource;
    }
    
    public TangoAttributeSourceDevice getAttributeSource(){
        return attributeSource;
    }
}
